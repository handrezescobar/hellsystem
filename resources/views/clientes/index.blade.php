@extends('layouts.clientes')
@section('content')
<div ng-app="clientes_app">
  	<div ng-controller="clientes_controlador">
		<section class="content-header">
		  <a ng-click="nuevoCliente()" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Cliente</a>
		  <h1>
		    Clientes
		    <small>Catálogo</small>
		  </h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th>Nombre</th>
									<th>Correo</th>
									<th>Teléfono</th>
									<th>RFC</th>
									<th>Direccion</th>
								</tr>
					        </thead>
								<tr ng-repeat="cliente in clientes">
									<td><a class="btn btn-circle btn-primary" title="Editar cliente" ng-click="fijarCliente(cliente)"><i class="fa fa-pencil"></i></a></td>
									<td>[[cliente.nombre]] [[cliente.apellido]]</td>
									<td>[[cliente.email]]</td>
									<td>[[cliente.telefono]]</td>
									<td>[[cliente.rfc]]</td>
									<td>[[cliente.direccion]], C.P. [[cliente.codigopostal]], [[cliente.ciudad]], [[cliente.estado]]</td>
								</tr>
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="cliente-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[cliente.id == 0 ? "Agregar Cliente": "Editar Cliente"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-cliente">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="cliente_id" ng-model="cliente.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated" placeholder="Ej: Juan" required ng-model="cliente.nombre">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Apellido</label>
										<div class="col-lg-10">
									  		<input type="text" name="apellido" class="form-control parsley-validated" placeholder="Ej: López" required ng-model="cliente.apellido">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Correo Electrónico</label>
										<div class="col-lg-10">
									  		<input type="email" name="email" class="form-control parsley-validated" placeholder="Ej: juan@mail.com" required ng-model="cliente.email">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Teléfono</label>
										<div class="col-lg-10">
									  		<input type="text" name="telefono" class="form-control parsley-validated" placeholder="Ej: 477 155 1777" required ng-model="cliente.telefono">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">R.F.C.</label>
										<div class="col-lg-10">
									  		<input type="text" name="rfc" class="form-control parsley-validated" placeholder="Ej: EOGT897812DJJDJ22" required ng-model="cliente.rfc">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Dirección</label>
										<div class="col-lg-10">
									  		<input type="text" name="direccion" class="form-control parsley-validated" placeholder="Ej: De La Arbide #455, col. Martinica" required ng-model="cliente.direccion">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Códio Postal</label>
										<div class="col-lg-10">
									  		<input type="text" name="codigop" class="form-control parsley-validated" placeholder="Ej: 37530" required ng-model="cliente.codigopostal">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Estado</label>
										<div class="col-lg-10">
									  		<select name="estado_id" ng-model="cliente.estado_id" class="form-control" id="estado-id" ng-change="obtenerCiudades()">
									  			<option ng-repeat="estado in estados" value="[[estado.id]]">[[estado.nombre]]</option>
									  		</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Ciudad</label>
										<div class="col-lg-10">
									  		<select name="ciudad_id" ng-model="cliente.ciudad_id" class="form-control" id="ciudad-id">
									  			<option ng-repeat="ciudad in ciudades" value="[[ciudad.id]]">[[ciudad.nombre]]</option>
									  		</select>
										</div>
									</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-cliente" value="Guardar" ng-click="guardarCliente()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
<script type="text/javascript">
	var AppAng = angular.module('clientes_app',[],function($interpolateProvider){
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');
	});
	AppAng.controller('clientes_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
		$scope.estados = [];
  		$scope.ciudades = [];
  		$http.get('catalogo/estados').then(function(response){$scope.estados = response.data},function(response){console.log(response.data)});
	  	$scope.obtenerCiudades = function(){
	  		angular.forEach($scope.estados, function(estado, index){
	  			if(estado.id == $scope.cliente.estado_id){
	  				$scope.ciudades = estado.ciudades;
	  				return;
	  			}
	  		});
	  	}
	    $scope.clientes = [];
	    $scope.cliente = {
	      id:0,
	      nombre:'',
	      apellido:'',
	      email:'',
	      telefono:'',
	      rfc:'',
	      direccion:'',
	      ciudad_id:'',
	      estado_id:'',
	      codigopostal:''
	    };
	    $scope.obtenerClientes = function(){
	      $http.get('clientes/listado').success(function(response){
	        $scope.clientes = response;
	      });
	    }
	    $scope.obtenerClientes();
	    $scope.nuevoCliente = function(){
	    	var cliente = {
		      id:0,
		      nombre:'',
		      apellido:'',
		      email:'',
		      telefono:'',
		      rfc:'',
		      direccion:'',
		      ciudad_id:'',
		      estado_id:'',
		      codigopostal:''
		    };
		    $scope.fijarCliente(cliente);
	    }
	    $scope.fijarCliente = function(cliente){
	    	$scope.cliente.id = cliente.id;
	    	$scope.cliente.nombre = cliente.nombre;
	    	$scope.cliente.apellido = cliente.apellido;
	    	$scope.cliente.email = cliente.email;
	    	$scope.cliente.telefono = cliente.telefono;
	    	$scope.cliente.rfc = cliente.rfc;
	    	$scope.cliente.direccion = cliente.direccion;
	    	$scope.cliente.estado_id = cliente.estado_id;
	    	$scope.cliente.ciudad_id = cliente.ciudad_id;
	    	$scope.cliente.codigopostal = cliente.codigopostal;
	    	$scope.obtenerCiudades();
	    	setTimeout(function(){$('#ciudad-id').val(cliente.ciudad_id);},300);
	    	$('#cliente-form').modal('show');
	    }
	    $scope.guardarCliente = function(){
	      $http({
	        url: 'clientes/guardar',
	        method: 'post',
	        data: $scope.cliente
	      }).success(function(response){
	        $scope.obtenerClientes();
	        if(response == "ok"){
	        	$('#cliente-form').modal('hide');
	        }
	        else{
	        	alert("Error al guardar");
	        }
	      });
	    }
	  }]);
</script>
@endsection