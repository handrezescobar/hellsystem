@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row row-centered">
        <div class="col-md-4 col-centered">
        <div class="login-logo">
            <img src="{{asset('img/logo_hellsystem.png')}}" height="100"><br>
            <!--<a href="#">Login</a><br>-->
        </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="login-box-msg">Inicia sesión para entrar a tu cuenta</p>
                    <form class="" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">

                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo Electrónico">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">

                                <input type="password" class="form-control" name="password" placeholder="Contraseña">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <!--<div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-flat btn-primary"><i class="glyphicon glyphicon-log-in"></i>  Login</button>

                                <!--<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
