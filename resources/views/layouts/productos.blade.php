<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@Hell_Systems</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.css')}}">
    <!-- css para wizard -->
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/wizard/prettify.css')}}">
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/angular.min.js')}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--COMPONENTES PARA PLUGIN SELECT2-->
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
    <link href="{{asset('plugins/pnotify/css/pnotify.custom.min.css')}}" rel="stylesheet">
    <script src="{{asset('plugins/pnotify/js/pnotify.custom.min.js')}}"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>@HS</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>@Hell_Systems</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" style="height: 50px;">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="height: 50px;">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->

              <!-- Notifications: style can be found in dropdown.less -->

              <!-- Tasks: style can be found in dropdown.less -->

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="height: 50px;">
                   <i class="fa fa-user"></i>
                  <span class="hidden-xs">{{ Auth::user()->nombre.' '.Auth::user()->appaterno }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <h1 style="color:#fff;"><i class="fa fa-user"></i></h1>
                    <p>
                      {{ Auth::user()->nombre.' '.Auth::user()->appaterno }}
                      <small>{{ Auth::user()->email }}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                    </div>
                    <div class="pull-right">
                      <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Cerrar sesión <i class="fa fa-power-off"></i></a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li class="header">MENÚ PRINCIPAL</li>
            <li>
              <a href="/">
                <i class="fa fa-home"></i>
                <span>Inicio</span>
              </a>
            </li>
            <li>
              <a href="/punto-venta">
                <i class="fa fa-money"></i>
                <span>Punto de Venta</span>
              </a>
            </li>
            <li>
              <a href="/punto-compra">
                <i class="fa fa-shopping-cart"></i>
                <span>Punto de Compra</span>
              </a>
            </li>
            <li>
              <a href="/almacen">
                <i class="fa fa-bar-chart"></i>
                <span>Almacén</span>
              </a>
            </li>
            <li>
              <a href="/usuarios">
                <i class="fa fa-users"></i>
                <span>Usuarios</span>
              </a>
            </li>
            <li class="active">
              <a href="/productos">
                <i class="fa fa-laptop"></i>
                <span>Productos</span>
              </a>
            </li>
            <li>
              <a href="/proveedores">
                <i class="fa fa-users"></i>
                <span>Proveedores</span>
              </a>
            </li>
            <li>
              <a href="/marcas">
                <i class="fa fa-tags"></i>
                <span>Marcas</span>
              </a>
            </li>
          </ul>
        </section>-->
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" id="content">
        @yield('content')
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
      </footer>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <!-- Slimscroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <script src="{{asset('js/jquery.selectedoption.plugin.js')}}"></script>
    <!-- Scripts para el plugin del wizard -->
    <script src="{{asset('plugins/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('plugins/wizard/prettify.js')}}"></script>
  </body>
</html>
