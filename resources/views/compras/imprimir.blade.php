@extends('compras.imprimir_template')
@section('content')
<div class="wrapper">        
  <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
            <img src="{{asset('img/logo_hellsystem.png')}}" width="150" class="pull-right">
            <h1>@Hell_Systems</h1><br><br><br>
              <h2 class="page-header">
                <i class="fa fa-suitcase"></i> Proveedor: {{$compra['proveedor_nombre']}}
                <small class="pull-right">Fecha: {{$compra['fecha']}}</small>
              </h2>
            </div>
            <!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                  <h3>Datos de proveedor</h3>
                  <address>
                    <b>Nombre: </b>{{$compra['proveedor_nombre']}}<br>
                    <b>Representante: </b>{{$compra['proveedor_representante']}}<br>
                    <b>R.F.C.: </b>{{$compra['proveedor_rfc']}}<br>
                    <b>Teléfono: </b>{{$compra['proveedor_telefono']}}<br>
                    <b>Correo: </b>{{$compra['proveedor_email']}}<br>
                    <b>Dirección: </b>{{$compra['proveedor_direccion']}}, C.P. {{$compra['proveedor_codigopostal']}}, {{$compra['ciudad']}}, {{$compra['estado']}}<br>
                  </address>
                </div>
                <div class="col-sm-4 invoice-col">
                  <h3>Datos de Usuario</h3>
                  <address>
                    <b>Nombre: </b>{{$compra['usuario']}}<br>
                    <b>Email: </b>{{$compra['usuario_email']}}<br>
                  </address>
                </div>
                <div class="col-sm-4 invoice-col" class="text-right">
                  <h3 class="text-right">Totales</h3>
                  <address class="text-right">
                    <h4>
                    <b>Subtotal: </b>$<span class="money">{{$compra['subtotal']}}</span><br><br>
                    <b>I.V.A.: </b>$<span class="money">{{$compra['iva']}}</span><br><br>
                    <b>Total: </b>$<span class="money">{{$compra['total']}}</span><br></h4>
                  </address>
                </div>
              </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <h5>Productos:</h5>
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Código</th>
                  <th>Descripción</th>
                  <th>Costo Unitario</th>
                  <th>Cantidad</th>
                  <th>Importe</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($productos as $index => $producto)
                    <tr>
                      <td>{{$producto['codigo']}}</td>
                      <td>{{$producto['descripcion']}}</td>
                      <td>$<span class="money">{{$producto['costo']}}</span></td>
                      <td>{{$producto['cantidad']}}</td>
                      <td>$<span class="money">{{$producto['importe']}}</span></td>
                    </tr>               
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
  </section>
   <div class="clearfix"></div>
</div><!-- /.content-wrapper -->
<footer class="main-footer">
</footer>
@endsection