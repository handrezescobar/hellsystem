@extends('layouts.compras')
@section('content')
<div ng-app="compras_app">
  <div ng-controller="compras_controlador">
    <section class="content-header">
      <a href="{{ url('compras/punto-compra') }}" class="btn btn-primary pull-right" style="margin-left:5px;"><i class="fa fa-plus"></i> Agregar Compra</a>
      <a href="{{ url('compras/reporte') }}" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Exportar Reporte</a>
      <h1>
        Resumen
        <small> Resumen de compras</small>
      </h1>
    </section>
    <section class="content">
      <div class="col-lg-12">
        <div class="row">
          <div class="panel">
            <div class="panel-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>Proveedor</th>
                    <th>Subtotal</th>
                    <th>I.V.A.</th>
                    <th>Total</th>
                    <th>Usuario</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="compra in compras">
                    <td>[[compra.proveedor]]</td>
                    <td>$[[compra.subtotal | number:2]]</td>
                    <td>$[[compra.iva | number:2]]</tdv>
                    <td>$[[compra.total | number:2]]</td>
                    <td>[[compra.usuario]]</td>
                    <td><a class="btn btn-primary btn-circle" ng-click="verCompra(compra)"><i class="fa fa-search"></i></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="modal fade" id="ver-compra-modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
            <h4 class="modal-title">Resumen de compra <a href="{{ url('/compras/imprimir/') }}/[[compra.id]]" class="btn btn-success pull-right" target="_blank"><i class="fa fa-print"></i> Imprimir factura</a></h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <h3>Datos de proveedor</h3>
                  <address>
                    <b>Nombre: </b>[[compra.proveedor_nombre]]<br>
                    <b>Representante: </b>[[compra.proveedor_representante]]<br>
                    <b>R.F.C.: </b>[[compra.proveedor_rfc]]<br>
                    <b>Teléfono: </b>[[compra.proveedor_telefono]]<br>
                    <b>Correo: </b>[[compra.proveedor_email]]<br>
                    <b>Dirección: </b>[[compra.proveedor_direccion]], C.P. [[compra.proveedor_codigopostal]], [[compra.proveedor_ciudad]], [[compra.proveedor_estado]]<br>
                  </address>
                </div>
                <div class="col-md-4">
                  <h3>Datos de Usuario</h3>
                  <address>
                    <b>Nombre: </b>[[compra.usuario]]<br>
                    <b>Email: </b>[[compra.usuario_email]]<br>
                  </address>
                </div>
                <div class="col-md-4" class="text-right">
                  <h3 class="text-right">Totales</h3>
                  <address class="text-right">
                    <h4>
                    <b>Subtotal: </b>$[[compra.subtotal | number:2]]<br><br>
                    <b>I.V.A.: </b>$[[compra.iva | number:2]]<br><br>
                    <b>Total: </b>$[[compra.total | number:2]]<br></h4>
                  </address>
                </div>
              </div>
              <div class="row">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Descripción</th>
                      <th>Costo Unitario</th>
                      <th>Cantidad</th>
                      <th>Importe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="producto in compra.productos track by $index">
                      <td>[[producto.codigo]]</td>
                      <td>[[producto.descripcion]]</td>
                      <td>$[[producto.costo | number:2]]</td>
                      <td>[[producto.cantidad]]</td>
                      <td>$[[producto.importe | number:2]]</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    </div>
  </div>
</div>
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/format-number/jquery.format-number.plugin.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('compras_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('compras_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $compras = [];
    $http.get('/compras/obtener-compras').then(function(response){console.log(response.data);$scope.compras = response.data},function(response){console.log(response.data);});
    $scope.compra;
    $scope.verCompra = function(compra){
      $http.get('/compras/obtener-compra/'+compra.id).then(
        function(response){
          console.log(response.data);
          $scope.compra = response.data;
          $('#ver-compra-modal').modal('show');
        });

    }

  }]);
</script>
@endsection