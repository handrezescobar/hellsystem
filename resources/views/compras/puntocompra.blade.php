@extends('layouts.puntocompra')
@section('content')
<div ng-app="puntocompra_app">
  <div ng-controller="puntocompra_controlador">
    <section class="content-header">
      <h1>
        Compras
        <small> Punto de compra</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-md-12 text-right">
              <a href="#" class="btn btn-primary" id="agregar-proveedor"><i class="fa fa-plus"></i> Seleccionar Proveedor</a>
              <a href="#" class="btn btn-primary" id="agregar-producto" ng-click="productosProveedor()"><i class="fa fa-plus"></i> Agregar Producto</a>
              <a href="#" class="btn btn-danger" ng-click="cancelarCompra()"><i class="fa fa-times"></i> Cancelar Compra</a>
            </div>
          </div>
          <h2 class="page-header">
            <i class="fa fa-suitcase"></i> [[compra.proveedor.nombre?compra.proveedor.nombre:'Selecciona proveedor']]
            <small class="pull-right">Fecha: <?php echo date('d/m/Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-6 invoice-col" ng-if="compra.proveedor.nombre">
            <h3>Datos de proveedor</h3>
            <address>
              <b>Nombre: </b>[[compra.proveedor.nombre]]<br>
              <b>Representante: </b>[[compra.proveedor.representante]]<br>
              <b>R.F.C.: </b>[[compra.proveedor.rfc]]<br>
              <b>Teléfono: </b>[[compra.proveedor.telefono]]<br>
              <b>Correo: </b>[[compra.proveedor.email]]<br>
              <b>Dirección: </b>[[compra.proveedor.direccion]], C.P. [[compra.proveedor.codigopostal]], [[compra.proveedor.ciudad]], [[compra.proveedor.estado]]<br>
            </address>            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Código</th>
              <th>Descripción</th>
              <th>Costo Unitario</th>
              <th>Cantidad</th>
              <th>Importe</th>
              <th></th>
              <th></th>
            </tr>
            </thead>
            <tbody>
              <tr ng-repeat="producto in compra.productos track by $index">
              	<td>[[producto.codigo]]</td>
              	<td>[[producto.descripcion]]</td>
              	<td>$[[producto.costo | number:2]]</td>
              	<td>[[producto.cantidad]]</td>
              	<td>$[[producto.importe | number:2]]</td>
                <td><a class="btn btn-primary btn-circle btn-sm" ng-click="editarProducto(producto, $index)"><i class="fa fa-pencil"></i></a></td>
              	<td><a class="btn btn-warning btn-circle btn-sm" ng-click="eliminarProducto($index)"><i class="fa fa-times"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Fecha de expedición <?php echo date('d/m/Y');?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>$[[compra.subtotal | number:2]]</td>
              </tr>
              <tr>
                <th>IVA (16%):</th>
                <td>$[[compra.iva | number:2]]</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>$[[compra.total | number:2]]</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <!--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>-->
          <a href="#" class="btn btn-success pull-right" ng-click="guardarCompra()"><i class="fa fa-credit-card"></i> Guardar Compra
          </a>
          <!--<button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>-->
        </div>
      </div>
    </section>
    <div class="modal fade" id="proveedor-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Selecciona Proveedor</h4>
          </div>
            <form class="form-horizontal" id="form-selecciona-proveedor">
              <div class="modal-body">
                  <fieldset>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Proveedor</label>
                        <div class="col-lg-10">
                          <select name="proveedor_id" class="form-control select-picker" ng-model="compra.proveedor_id" data-live-search="true" data-live-search-placeholder="Selecciona un proveedor" data-live-search-style="begins" title="Selecciona un proveedor">
                            <option value="">Selecciona un proveedor</option>
                            <option ng-repeat="proveedor in proveedores" data-proveedor="[[proveedor]]" value="[[proveedor.id]]">[[proveedor.nombre]]</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Agregar Proveedor" name="agrega_proveedor" ng-click="seleccionarProveedor()">
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="producto-modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Selecciona Producto</h4>
          </div>
            <form class="form-horizontal" id="form-agrega-producto" method="post" action="puntocompra.php">
              <div class="modal-body">
                  <div class="col-md-12">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Descripción</th>
                          <th>Código</th>
                          <th>Marca</th>
                          <th>Categoría</th>
                          <th>Línea de producto</th>
                          <th>Costo</th>
                          <th>Existencia</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="producto in productos">
                          <td>[[producto.descripcion]]</td>
                          <td>[[producto.codigo]]</td>
                          <td>[[producto.marca]]</td>
                          <td>[[producto.categoria]]</td>
                          <td>[[producto.lineaproducto]]</td>
                          <td>$[[producto.costo | number:2]]</td>
                          <td>[[producto.existencia]]</td>
                          <td><a href="#" class="btn btn-circle btn-primary" ng-click="seleccionaProducto(producto)"><i class="fa fa-plus"></i></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="aproducto-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Cantidad de producto [[aproducto.descripcion]]</h4>
          </div>
            <form class="form-horizontal" id="form-cantidad">
              <div class="modal-body">
                  <fieldset>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Cantidad</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" ng-model="aproducto.cantidad" placeholder="0" ng-keyup="calcularImporte()" data-parsley-type="number" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Costo</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="aproducto.costo">
                          <h4>$<span>[[aproducto.costo | number:2]]</span></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Importe</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="aproducto.importe">
                          <h4>$<span>[[aproducto.importe | number:2]]</span></h4>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Agregar Producto" name="agrega_proveedor" ng-click="agregarProducto()">
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="eaproducto-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Editar cantidad de producto [[aproducto.descripcion]]</h4>
          </div>
            <form class="form-horizontal" id="form-edita">
              <div class="modal-body">
                  <fieldset>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Cantidad</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" ng-model="eaproducto.cantidad" placeholder="0" ng-keyup="calcularImporteE()" data-parsley-type="number" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Costo</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="eaproducto.costo">
                          <h4>$<span>[[eaproducto.costo | number:2]]</span></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Importe</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="eaproducto.importe">
                          <h4>$<span>[[eaproducto.importe | number:2]]</span></h4>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Guardar Producto" ng-click="guardarEdicionProducto()">
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ver-compra-modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
            <h4 class="modal-title">Resumen de compra <a href="{{ url('/compras/imprimir/') }}/[[comprav.id]]" class="btn btn-success pull-right" target="_blank"><i class="fa fa-print"></i> Imprimir factura</a></h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <h3>Datos de proveedor</h3>
                  <address>
                    <b>Nombre: </b>[[comprav.proveedor_nombre]]<br>
                    <b>Representante: </b>[[comprav.proveedor_representante]]<br>
                    <b>R.F.C.: </b>[[comprav.proveedor_rfc]]<br>
                    <b>Teléfono: </b>[[comprav.proveedor_telefono]]<br>
                    <b>Correo: </b>[[comprav.proveedor_email]]<br>
                    <b>Dirección: </b>[[comprav.proveedor_direccion]], C.P. [[comprav.proveedor_codigopostal]], [[comprav.proveedor_ciudad]], [[comprav.proveedor_estado]]<br>
                  </address>
                </div>
                <div class="col-md-4">
                  <h3>Datos de Usuario</h3>
                  <address>
                    <b>Nombre: </b>[[comprav.usuario]]<br>
                    <b>Email: </b>[[comprav.usuario_email]]<br>
                  </address>
                </div>
                <div class="col-md-4" class="text-right">
                  <h3 class="text-right">Totales</h3>
                  <address class="text-right">
                    <h4>
                    <b>Subtotal: </b>$[[comprav.subtotal | number:2]]<br><br>
                    <b>I.V.A.: </b>$[[comprav.iva | number:2]]<br><br>
                    <b>Total: </b>$[[comprav.total | number:2]]<br></h4>
                  </address>
                </div>
              </div>
              <div class="row">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Descripción</th>
                      <th>Costo Unitario</th>
                      <th>Cantidad</th>
                      <th>Importe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="producto in comprav.productos track by $index">
                      <td>[[producto.codigo]]</td>
                      <td>[[producto.descripcion]]</td>
                      <td>$[[producto.costo | number:2]]</td>
                      <td>[[producto.cantidad]]</td>
                      <td>$[[producto.importe | number:2]]</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
  </div>
</div>
<script type="text/javascript" src="{{asset('js/parsley.js')}}"></script>
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/format-number/jquery.format-number.plugin.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#agregar-proveedor').click(function(){
      $('#proveedor-modal').modal('show');
    });
    $('#form-cantidad').parsley('validate');
    //$('.select-picker').selectpicker();
    /*$('#agregar-producto').click(function(){
      $('#producto-modal').modal('show');
    });*/
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('puntocompra_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('puntocompra_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $scope.compra = {
      proveedor_id:'',
      proveedor: {},
      productos:[],
      subtotal:0.0,
      iva:0.0,
      total:0.0
    };
    //$scope.proveedores = [];
    $scope.productos = [];
    $http.get('/proveedores/listado').then(
      function(response){
        console.log(response.data);
        $scope.proveedores = response.data;
      },
      function(response){
        console.log(response.data);
      });
    $scope.seleccionarProveedor = function(){
      angular.forEach($scope.proveedores, function(proveedor, index){
        if($scope.compra.proveedor_id == proveedor.id){
          $scope.cancelarCompra();
          $scope.compra.proveedor = proveedor;
          $scope.compra.proveedor_id = proveedor.id
          return false;
        } 
      });
      console.log($scope.compra);
      $('#proveedor-modal').modal('hide');
    }
    $scope.productosProveedor = function(){
      if(!$scope.compra.proveedor.nombre){
        alert("Selecciona un proveedor para mostrar los productos disponibles");
        return;
      }
      $http.get('/compras/obtener-productos/'+$scope.compra.proveedor.id).then(function(response){console.log(response.data);$scope.productos = response.data},function(response){console.log(response.data);});
      $('#producto-modal').modal('show');
    }
    $scope.aproducto;
    $scope.seleccionaProducto = function(producto){
      $scope.aproducto = producto;
      $scope.aproducto.cantidad = '';
      $scope.aproducto.importe = 0;
      console.log($scope.aproducto);
      $('#aproducto-modal').modal('show');
    }
    $scope.calcularImporte = function(){
      $scope.aproducto.importe = $scope.aproducto.cantidad * $scope.aproducto.costo;
      console.log($scope.aproducto.importe);
    }
    $scope.calcularImporteE = function(){
      $scope.eaproducto.importe = $scope.eaproducto.cantidad * $scope.eaproducto.costo;
      console.log($scope.eaproducto.importe);
    }
    $scope.agregarProducto = function(){
      $('#form-cantidad').parsley();
      $('#form-cantidad').submit();
      $('#form-cantidad').parsley('reset');
      if($('#form-cantidad').parsley('validate').validationResult){
        $scope.compra.productos.push($scope.aproducto);
        $scope.totales();
        $('#aproducto-modal').modal('hide'); 
        $('#producto-modal').modal('hide');
      }
    }
    $scope.eaproducto;
    $scope.editarProducto = function(producto, index){
      $scope.eaproducto = producto;
      $scope.eaproducto.index = index;
      $('#eaproducto-modal').modal('show');
    }
    $scope.guardarEdicionProducto = function(){
      $('#form-edita').parsley();
      $('#form-edita').submit();
      $('#form-edita').parsley('reset');
      if($('#form-edita').parsley('validate').validationResult){
        $scope.compra.productos[$scope.eaproducto.index] = $scope.eaproducto;
        $scope.totales();
        $('#eaproducto-modal').modal('hide');
      } 
    }
    $scope.eliminarProducto = function(index){
      if(confirm("¿Estás seguro de querer eliminar el producto de la compra?"))
        $scope.compra.productos.splice(index,1);
      $scope.totales();
    }
    $scope.totales = function(){
      $scope.compra.subtotal = 0;
      angular.forEach($scope.compra.productos, function(producto, index){
        $scope.compra.subtotal += producto.importe;
      });
      $scope.compra.iva = $scope.compra.subtotal*.16;
      $scope.compra.total = $scope.compra.subtotal + $scope.compra.iva;
    }
    $scope.cancelarCompra = function(){
      $scope.compra.proveedor_id = '';
      $scope.compra.proveedor = {};
      $scope.compra.productos = [];
      $scope.compra.subtotal = 0.0;
      $scope.compra.iva = 0.0;
      $scope.compra.total = 0.0;
    }
    $scope.guardarCompra = function(){
      if(!$scope.compra.proveedor.nombre){
        alert("Comienza seleccionando un proveedor");
        return;
      }
      if($scope.compra.productos.length <= 0){
        alert("Agrega por lo menos un producto a la compra");
        return;
      }
      $scope.comprav;
      $http.post('/compras/guardar-compra',$scope.compra).then(
        function(response){
          console.log(response.data);
          if(response.data != 'error'){
            var compra_id = response.data;
            alert('Se guardo con éxito la compra');
            $http.get('/compras/obtener-compra/'+compra_id).then(
              function(response){
                console.log(response.data);
                $scope.comprav = response.data;
                $('#ver-compra-modal').modal('show');
              });
            $scope.cancelarCompra();

          }
        },function(response){
          console.log(response.data);
        });
    }
  }]);
</script>
@endsection