@extends('layouts.usuarios')
@section('content')
<div ng-app="usuarios_app">
  	<div ng-controller="usuarios_controlador">
		<section class="content-header">
		  <a ng-click="nuevoUsuario()" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Usuario</a>
		  <h1>
		    Usuarios
		    <small>de sistema</small>
		  </h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th></th>
									<th>Nombre</th>
									<th>Correo</th>
									<th>Creado</th>
								</tr>
					        </thead>
								<tr ng-repeat="usuario in usuarios">
									<td><a class="btn btn-circle btn-primary" title="Editar usuario" ng-click="fijarUsuario(usuario)"><i class="fa fa-pencil"></i></a></td>
									<td><a class="btn btn-circle btn-primary" title="Restaurar contraseña"  ng-click="restaurarPassword(usuario)"><i class="fa fa-key"></i></a></td>
									<td>[[usuario.nombre]] [[usuario.apellido]]</td>
									<td>[[usuario.email]]</td>
									<td>[[usuario.created_at]]</td>
								</tr>
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="usuario-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[usuario.id == 0 ? "Agregar Usuario": "Editar Usuario"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-usuario" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="usuario_id" ng-model="usuario.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated" placeholder="Ej: Juan" required ng-model="usuario.nombre">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Apellido</label>
										<div class="col-lg-10">
									  		<input type="text" name="apellido" class="form-control parsley-validated" placeholder="Ej: López" required ng-model="usuario.apellido">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Correo Electrónico</label>
										<div class="col-lg-10">
									  		<input type="email" name="email" class="form-control parsley-validated" placeholder="Ej: juan@mail.com" required ng-model="usuario.email">
										</div>
									</div>
									<div class="form-group" ng-show="usuario.id != 0">
										<label class="col-lg-2 control-label">Estatus</label>
										<div class="col-lg-10">
											<label>
												<input type="checkbox" ng-model="usuario.estatus"> [[usuario.estatus == 1 ? 'Activo': 'Inactivo']]
											</label>
										</div>
									</div>
									<div class="form-group" ng-show="usuario.id == 0">
										<label class="col-lg-2 control-label">Contraseña</label>
										<div class="col-lg-10">
									  		<input type="password" name="password" class="form-control parsley-validated" required ng-model="passwords.pass1">
										</div>
									</div>
									<div class="form-group" ng-show="usuario.id == 0">
										<label class="col-lg-2 control-label">Confirmar contraseña</label>
										<div class="col-lg-10">
									  		<input type="password" name="passwordconf" class="form-control parsley-validated" required ng-model="passwords.pass2">
										</div>
									</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-usuario" value="Guardar" ng-click="guardarUsuario()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="usuario-password-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">Restaurar contraseña de usuario <br>[[usuario.nombre]] [[usuario.apellido]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-usuario">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Contraseña</label>
										<div class="col-lg-10">
										<input type="hidden" name="usuario_id" ng-model="usuario.id">
									  		<input type="password" name="password" class="form-control parsley-validated" required ng-model="passwords.pass1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Confirmar contraseña</label>
										<div class="col-lg-10">
									  		<input type="password" name="passwordconf" class="form-control parsley-validated" required ng-model="passwords.pass2">
										</div>
									</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-usuario" value="Guardar" ng-click="guardarPassword()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
    $('#agregar-usuario').click(function(){
      $('#usuario-form').modal('show');
    });
    $('#registrar-usuario').click(function(e){
      e.preventDefault();
    });
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('usuarios_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('usuarios_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $scope.usuarios = [];
    $scope.usuario = {
      id:0,
      nombre:'',
      apellido:'',
      email:''
    };
    $scope.passwords = {
    	pass1:'',
    	pass2:''
    }
    $scope.obtenerUsuarios = function(){
      $http.get('usuarios/listado').success(function(response){
        $scope.usuarios = response;
      });
    }
    $scope.obtenerUsuarios();
    $scope.nuevoUsuario = function(){
    	var usuario = {
	      id:0,
	      nombre:'',
	      apellido:'',
	      email:''
	    };
	    $scope.passwords.pass1 = '';
	    $scope.passwords.pass2 = '';
	    $scope.fijarUsuario(usuario);
    }
    $scope.fijarUsuario = function(usuario){
    	$scope.usuario.id = usuario.id;
    	$scope.usuario.nombre = usuario.nombre;
    	$scope.usuario.apellido = usuario.apellido;
    	$scope.usuario.email = usuario.email;
    	$('#usuario-form').modal('show');
    }
    $scope.guardarUsuario = function(){
      var datos = $scope.usuario.id == 0 ? {usuario_n:$scope.usuario,password: $scope.passwords.pass1} : {usuario_e:$scope.usuario};
      if($scope.usuario.id == 0){
      	if($scope.validarPasswords() == false){
  			return;
      	}
      }
      $http({
        url: 'usuarios/guardar',
        method: 'post',
        data: datos
      }).success(function(response){
        $scope.obtenerUsuarios();
        if(response == "ok")
        	$('#usuario-form').modal('hide');
        else
        	alert("Error al guardar");
      });
    }
    $scope.restaurarPassword = function(usuario){
    	$scope.usuario.id = usuario.id;
    	$scope.usuario.nombre = usuario.nombre;
    	$scope.usuario.apellido = usuario.apellido;
    	$scope.passwords.pass1 = '';
	    $scope.passwords.pass2 = '';
    	$('#usuario-password-form').modal('show');
    }
    $scope.validarPasswords = function(){
    	if($scope.passwords.pass1 == $scope.passwords.pass2){
    		return true;
    	} else {
    		alert("Las contraseñas no coinciden");
    		return false;		
    	}
    }
    $scope.guardarPassword = function(){
    	if($scope.validarPasswords() == true){
    		console.log("LOL");
    		$http({
		        url: 'usuarios/restaurar-password',
		        method: 'post',
		        data: {passwords:$scope.passwords,usuario_id:$scope.usuario.id}
		      }).success(function(response){
		      	if(response == "ok"){
		      		alert("Se restauró la contraseña con éxito");
		      		$scope.obtenerUsuarios();
		        	$('#usuario-password-form').modal('hide');
		      	} else {
		      		alert(response);
		      	}
		        
		      });
    	}
    }
  }]);
</script>
@endsection