@extends('layouts.usuarios')
@section('content')
<div ng-app="usuarios_app">
  	<div ng-controller="usuarios_controlador">
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				   	<div class="col-md-2"></div>
				   	<div class="col-md-8">
					  	<div class="panel">
					  		<div class="panel-heading">
					  			<h1>Agregar<small> Usuario</small></h1>
					  		</div>
						    <div class="panel-body">
						      <form action="guardar-usuario">
						      	{!! csrf_field() !!}
						      	<div class="form-group">
						      		<label for="nombre">Nombre</label>
						      		<input type="text" name="nombre" class="form-control" placeholder="Ej: Juan">
						      	</div>
						      	<div class="form-group">
						      		<label for="nombre">Apellido</label>
						      		<input type="text" name="apellido" class="form-control" placeholder="Ej: López">
						      	</div>
						      	<div class="form-group">
						      		<label for="nombre">Correo Electrónico</label>
						      		<input type="email" name="email" class="form-control" placeholder="Ej: juan@hotmail.com">
						      	</div>
						      	<div class="form-group">
						      		<label for="nombre">Contraseña</label>
						      		<input type="password" name="password" class="form-control">
						      	</div>
						      	<div class="form-group">
						      		<label for="nombre">Confirmar Contraseña</label>
						      		<input type="password" name="passwordconf" class="form-control">
						      	</div>
						      	<input type="submit" value="Guardar" class="btn btn-primary pull-right" style="margin-left:5px;">
						      	<input type="reset" value="Limpiar" class="btn btn-danger pull-right">
						      	<a class="btn btn-primary" href="/usuarios"><i class="fa fa-arrow-left"></i> Regresar</a>
						      </form>
						    </div>
					  	</div>
				  	</div>
				  	<div class="col-md-2"></div>
			  	</div>
			</div>
		</section>
		<div class="modal fade" id="usuarios-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[usuario.id == 0 ? "Agregar Usuario": "Editar Usuario"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-usuario" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="usuario_id" ng-model="usuario.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated" placeholder="Ej: Juan" required ng-model="usuario.nombre">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Apellido</label>
										<div class="col-lg-10">
									  		<input type="text" name="apellido" class="form-control parsley-validated" placeholder="Ej: López" required ng-model="usuario.apellido">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Correo Electrónico</label>
										<div class="col-lg-10">
									  		<input type="email" name="email" class="form-control parsley-validated" placeholder="Ej: juan@mail.com" required ng-model="usuario.email">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Contraseña</label>
										<div class="col-lg-10">
									  		<input type="password" name="password" class="form-control parsley-validated" required ng-model="usuario.password">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Confirmar contraseña</label>
										<div class="col-lg-10">
									  		<input type="password" name="passwordconf" class="form-control parsley-validated" required ng-model="usuario.passwordconf">
										</div>
									</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-usuario" value="Guardar" ng-click="guardarUsuario()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
@endsection