@extends('layouts.almacen')
@section('content')
<div ng-app="productos_app">
  	<div ng-controller="productos_controlador">
		<section class="content-header">	
		  	<a ng-click="nuevoProducto()" class="btn btn-primary pull-right" style="margin-left:5px;"><i class="fa fa-plus"></i> Agregar Producto</a>
			<a href="{{ url('almacen/reporte') }}" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Exportar Reporte</a>
			<h1>Almácen
				<small>Inventario de Productos</small>
			</h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table id="tabla-productos" class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th>Descripción</th>
									<th>Código</th>
									<th>Marca</th>
									<th>Categoría</th>
									<th>Línea de producto</th>
									<th>Existencia</th>
									<th>Activo</th>
								</tr>
					        </thead>
					        <tbody>
								<tr ng-repeat="producto in productos" on-finish-render="ngRepeatFinished">
									<td>
										<a class="btn btn-circle btn-primary" title="Editar producto" ng-click="fijarProducto(producto)"><i class="fa fa-pencil"></i></a> 
										<a class="btn btn-circle btn-primary" title="Editar producto" ng-click="costosProducto(producto)"><i class="fa fa-dollar"></i></a> 
										<a class="btn btn-circle btn-primary" title="Editar producto" ng-click="verProducto(producto)"><i class="fa fa-search"></i></a>
									</td>
									<td>[[producto.descripcion]]</td>
									<td>[[producto.codigo]]</td>
									<td>[[producto.marca]]</td>
									<td>[[producto.categoria]]</td>
									<td>[[producto.lineaproducto]]</td>
									<td>[[producto.existencia]]</td>
									<td>[[producto.estatus == 1 ? 'Activo' : 'Inactivo']]</td>
								</tr>
							</tbody>								
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="producto-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[producto.id == 0 ? "Agregar producto": "Editar producto"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-producto" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Descripción</label>
										<div class="col-lg-10">
											<input type="hidden" name="producto_id" ng-model="producto.id">
									  		<textarea class="form-control" ng-model="producto.descripcion" placeholder="Ej: Tarjeta de video NvidiaGTX2121..."></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Código</label>
										<div class="col-lg-10">
									  		<input type="text" class="form-control parsley-validated" placeholder="Ej: PRO121212" required ng-model="producto.codigo">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Marca</label>
										<div class="col-lg-10">
									  		<select class="form-control" ng-model="producto.marca_id">
									  			<option value="">Selecciona una marca</option>
									  			<option ng-repeat="marca in marcas" value="[[marca.id]]">[[marca.nombre]]</option>
									  		</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Categoría</label>
										<div class="col-lg-10">
									  		<select class="form-control" ng-model="producto.categoria_id">
									  			<option value="">Selecciona una categoría</option>
									  			<option ng-repeat="categoria in categorias" value="[[categoria.id]]">[[categoria.nombre]]</option>
									  		</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Linea de producto</label>
										<div class="col-lg-10">
									  		<select class="form-control" ng-model="producto.lineaproducto_id">
									  			<option value="">Selecciona una línea de producto</option>
									  			<option ng-repeat="linea in lineas" value="[[linea.id]]">[[linea.abreviatura]]-[[linea.nombre]]</option>
									  		</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Estatus</label>
										<div class="col-lg-10">
											<label>
									  			<input type="checkbox" class="parsley-validated" required ng-model="producto.estatus"> [[producto.estatus == true?'Activo':'Inactivo']]
									  		</label>
										</div>
									</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-producto" value="Guardar" ng-click="guardarProducto()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="costos-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">Costos de producto</h4>
					</div>
					<div class="modal-body">
						<div class="col-md-12">
							<div class="col-md-6">
								<form>
									<input type="hidden" ng-model="costo.id">
									<input type="hidden" ng-model="costo.producto_id">
									<div class="form-group">
										<label>Proveedor</label>
										<select class="form-control" ng-model="costo.proveedor_id">
											<option value="0">Selecciona un proveedor</option>
											<option ng-repeat="proveedor in proveedores" value="[[proveedor.id]]">[[proveedor.nombre]]</option>
										</select>
									</div>
									<div class="form-group">
										<label>Costo</label>
										<div class="input-group">
											<span class="input-group-addon">$</span>
											<input type="text" class="form-control" ng-model="costo.costo" placeholder="0.00">
										</div>
									</div>
									<div class="pull-right">
										<input type="submit" value="[[costo.id == 0?'Agregar':'Guardar']]" ng-click="guardarCosto()" class="btn btn-primary" style="margin-left:5px;">
										<input type="reset" value="Cancelar" ng-click="resetCosto()" class="btn btn-danger">
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<table class="table">
									<thead>
										<tr>
											<th>Proveedor</th>
											<th>Costo</th>
											<th></th>
										</tr>
									</thead>
									<thead>
										<tr ng-repeat="costo in costos">
											<td>[[costo.proveedor]]</td>
											<td>$<span class="money">[[costo.precio]]</span></td>
											<td><a class="btn btn-circle btn-primary" ng-click="editarCosto(costo)"><i class="fa fa-pencil"></i></a> <a class="btn btn-circle btn-danger" ng-click="eliminarCosto(costo)"><i class="fa fa-trash"></i></a></td>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				  	<div class="modal-footer">
				  		<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				  	</div>
			  	</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="ver-producto">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h3 class="modal-title">Información de producto</h3>
					</div>
					<div class="modal-body">
						<div class="col-md-12">
							<div class="col-md-6">
								<strong>Descripción</strong>
								<p class="text-muted">[[producto.descripcion]]</p>
								<strong>Código</strong>
								<p class="text-muted">[[producto.codigo]]</p>
								<strong>Marca</strong>
								<p class="text-muted">[[producto.marca]]</p>
								<strong>Categoría</strong>
								<p class="text-muted">[[producto.categoria]]</p>
								<strong>Linea de producto</strong>
								<p class="text-muted">[[producto.lineaproducto]]</p>
								<strong>Existencia</strong>
								<p class="text-muted">[[producto.existencia]]</p>
								<strong>Estatus</strong>
								<p class="text-muted">[[producto.estatus == true?'Activo':'Inactivo']]</p>
							</div>
							<div class="col-md-6">
								<h4>Costos de producto</h4>
								<table class="table">
									<thead>
										<tr>
											<th>Proveedor</th>
											<th>Costo</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="costo in costos">
											<td>[[costo.proveedor]]</td>
											<td>$<span class="money">[[costo.costo]]</span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				  	<div class="modal-footer">
				  		<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
				  	</div>
			  	</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
<script type="text/javascript" src="{{asset('plugins/format-number/jquery.format-number.plugin.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
    $('#agregar-producto').click(function(){
      $('#producto-form').modal('show');
    });
    $('#registrar-producto').click(function(e){
      e.preventDefault();
    });
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('productos_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  }).directive('onFinishRender', function ($timeout) {
		    return {
		        restrict: 'A',
		        link: function (scope, element, attr) {
		            if (scope.$last === true) {
		                $timeout(function () {
		                    scope.$emit('ngRepeatFinished');
		                });
		            }
		        }
		    }
		});;
  AppAng.controller('productos_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
  	$scope.productos = [];
  	$scope.marcas = [];
  	$scope.categorias = [];
  	$scope.lineas = [];
  	$scope.costos = [];
  	$scope.proveedores = [];
  	$scope.producto = {
	      id:0,
	      descripcion:'',
	      codigo:'',
	      categoria_id:'',
	      categoria:'',
	      marca_id:'',
	      marca:'',
	      lineaproducto_id:'',
	      lineaproducto:'',
	      existencia:'',
	      estatus:0
	    };
    $scope.costo = {
    	id:0,
    	producto_id:0,
    	proveedor_id:0,
    	costo:''
    };
  	$scope.obtenerProductos = function(){
  		$http.get('/almacen/obtener-productos').then(function(response){console.log(response.data);$scope.productos = response.data;},function(response){console.log(response.data);});
  	}
  	$http.get('/marcas/listado').then(function(response){$scope.marcas = response.data;},function(response){console.log(response.data);});
  	$http.get('/categorias/listado').then(function(response){$scope.categorias = response.data;},function(response){console.log(response.data);});
  	$http.get('/lineasproducto/listado').then(function(response){$scope.lineas = response.data;},function(response){console.log(response.data);});
  	$http.get('/proveedores/listado').then(function(response){$scope.proveedores = response.data;},function(response){console.log(response.data);});
  	$scope.obtenerProductos();
  	$scope.nuevoProducto = function(){
    	var producto = {
	      id:0,
	      descripcion:'',
	      codigo:'',
	      categoria_id:'',
	      marca_id:'',
	      lineaproducto_id:'',
	      estatus:0
	    };
	    $scope.fijarProducto(producto);
    }
    $scope.fijarProducto = function(producto){
    	console.log(producto);
    	$scope.producto.id = producto.id;
    	$scope.producto.descripcion = producto.descripcion;
    	$scope.producto.codigo = producto.codigo;
    	$scope.producto.marca_id = producto.marca_id;
    	$scope.producto.lineaproducto_id = producto.lineaproducto_id;
    	$scope.producto.categoria_id = producto.categoria_id;
    	$scope.producto.estatus = producto.estatus==1?true:false;
    	$('#producto-form').modal('show');
    }
    $scope.guardarProducto = function(){
    	$http({
    		url: 'almacen/guardar-producto',
    		method: 'post',
    		data: $scope.producto
    	}).then(function(response){
    		if(response.data == 'ok'){
    			$scope.obtenerProductos();
    			$('#producto-form').modal('hide');
    		}
    	});
    }
    $scope.costosProducto = function(producto){
    	$scope.obtenerCostos(producto.id);
    	$scope.costo.producto_id = producto.id;
    	$('#costos-form').modal('show');
    }
    $scope.guardarCosto = function(){
    	$http({
    		url: 'almacen/guardar-costo',
    		method: 'post',
    		data: $scope.costo
    	}).then(function(response){
    		if(response.data == 'ok'){
    			$scope.obtenerCostos($scope.costo.producto_id);
    			$scope.resetCosto();
    		}
    	});
    }
    $scope.obtenerCostos = function(producto_id){
    	$http.get('almacen/obtener-costos/'+producto_id).then(
    		function(response){
    			console.log(response.data);
    			$scope.costos = response.data;
    		});
    	setTimeout(function(){$('.money').formatNumber()},300);
    }
    $scope.resetCosto = function(){
    	$scope.costo.id = 0;
    	$scope.costo.proveedor_id = 0;
    	$scope.costo.costo = '';
    }
    $scope.editarCosto = function(costo){
    	console.log(costo);
    	$scope.costo.id = costo.id;
    	$scope.costo.proveedor_id = costo.proveedor_id;
    	$scope.costo.costo = costo.precio;
    }
    $scope.eliminarCosto = function(costo){
    	if(confirm("¿Está seguro de querer eliminar este costo?")){
    		$http.get('almacen/eliminar-costo/'+costo.id).then(function(response){
    			if (response.data=='ok') {
    				$scope.obtenerCostos(costo.producto_id);
    			}
    		});
    	}
    }
    $scope.verProducto = function(producto){
    	$scope.producto.descripcion = producto.descripcion;
    	$scope.producto.codigo = producto.codigo;
    	$scope.producto.marca = producto.marca;
    	$scope.producto.categoria = producto.categoria;
    	$scope.producto.lineaproducto = producto.lineaproducto;
    	$scope.producto.existencia = producto.existencia;
    	$scope.producto.estatus = producto.estatus==1?true:false;
    	$scope.obtenerCostos(producto.id);
    	$('#ver-producto').modal('show');
    }
    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
    	//$('#tabla-productos').DataTable();
    	setTimeout(function(){
    		$('#tabla-productos').DataTable();
    	},300);
    });
  }]);
</script>
@endsection