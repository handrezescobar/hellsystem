<table border="1">
    <thead>
        <tr>
            <th>Descripción</th>
            <th>Código</th>
            <th>Costo</th>
            <th>Existencias</th>
            <th>Marca</th>
            <th>Categoría</th>
            <th>Proveedor</th>
            <th>Fecha de registro</th>
            <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
        @foreach($productos AS $index => $producto)
        <tr>
            <td>{{$producto['descripcion']}}</td>
            <td>{{$producto['codigo']}}</td>
            <td>{{$producto['costo']}}</td>
            <td>{{$producto['stock']}}</td>
            <td>{{$producto['marca']}}</td>
            <td>{{producto['categoria']}}</td>
            <td>{{$producto['proveedor']}}</td>
            <td>{{$producto['fecharegistro']}}</td>
            <td>{{($producto['activo']==1?'Activo':'Inactivo')}}</td>
        </tr>
    </tbody>
</table>