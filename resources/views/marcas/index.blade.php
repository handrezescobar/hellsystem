@extends('layouts.marcas')
@section('content')
	<div ng-app="marcas_app">
  	<div ng-controller="marcas_controlador">
		<section class="content-header">
		  <a ng-click="nuevaMarca()" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Marca</a>
		  <h1>
		    Marcas
		    <small>de productos</small>
		  </h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th></th>
									<th>Nombre</th>
									
								</tr>
					        </thead>
								<tr ng-repeat="marca in marcas">
									<td><a class="btn btn-circle btn-primary" title="Editar Marca" ng-click="fijarMarca(marca)"><i class="fa fa-pencil"></i></a></td>
									<td><a class="btn btn-circle btn-primary" title="Elimnar Marca"  ng-click="eliminarMarca(marca)"><i class="fa fa-trash"></i></a></td>
									<td>[[marca.nombre]]</td>
		
								</tr>
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="marca-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[marca.id == 0 ? "Agregar marca": "Editar marca"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-marca" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="marca_id" ng-model="marca.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated" placeholder="Ej: Sony" required ng-model="marca.nombre">
										</div>
									</div>
									
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-marca" value="Guardar" ng-click="guardarMarca()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div> 
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
    $('#agregar-marca').click(function(){
      $('#marca-form').modal('show');
    });
    $('#registrar-marca').click(function(e){
      e.preventDefault();
    });
  });
</script>

<script type="text/javascript">
  var AppAng = angular.module('marcas_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('marcas_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $scope.marcas = [];
    $scope.marca = {
      id:0,
      nombre:''
    };
    //Funcion para traer registro de marcas
    $scope.obtenerMarcas = function(){
      $http.get('marcas/listado').success(function(response){
        $scope.marcas = response;
      });
    }
    $scope.obtenerMarcas();
    $scope.nuevaMarca = function(){
    	var marca = {
      id:0,
      nombre:''
    };
	    
	    $scope.fijarMarca(marca);
    }
    $scope.fijarMarca = function(marca){
    	$scope.marca.id = marca.id;
    	$scope.marca.nombre = marca.nombre;
    	$('#marca-form').modal('show');
    }
    $scope.guardarMarca = function(){
      $http({
        url: 'marcas/guardar',
        method: 'post',
        data: $scope.marca
      }).success(function(response){
        $scope.obtenerMarcas();
        if(response == "ok")
        	$('#marca-form').modal('hide');
        else
        	alert("Error al guardar");
      });
    }
     $scope.eliminarMarca = function(marca){
     	if(confirm("?Estas seguro de querer eliminar la marca?")){
     			$scope.marca.id = marca.id;
     	$http({
     		url:'marcas/eliminar',
     		method: 'post',
     		data: $scope.marca
     	}).then(function(response){
     		if(response.data == 'ok'){
     				alert('Se elimino con exito');
     				$scope.obtenerMarcas();
 			} else {
 				alert('Error');
 			}
     	},function(response){console.log(response.data)});
     	}
     	
     }
  }]);
</script>

@endsection