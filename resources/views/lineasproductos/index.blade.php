@extends('layouts.lineasproducto')
@section('content')
<div ng-app="lineasproducto_app">
  	<div ng-controller="lineasproducto_controlador">
		<section class="content-header">
		  <a ng-click="nuevaLineaproducto()" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Línea de producto</a>
		  <h1>
		    Líneas
		    <small>de producto</small>
		  </h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th></th>
									<th>Nombre</th>
									<th>Abreviatura</th>
								</tr>
					        </thead>
								<tr ng-repeat="lineaproducto in lineasproducto">
									<td><a class="btn btn-circle btn-primary" title="Editar linea de producto" ng-click="fijarLineaproducto(lineaproducto)"><i class="fa fa-pencil"></i></a></td>
									<td><a class="btn btn-circle btn-primary" title="Eliminar linea de producto"  ng-click="eliminarLineaproducto(lineaproducto)"><i class="fa fa-trash"></i></a></td>
									<td>[[lineaproducto.nombre]] </td>
									<td>[[lineaproducto.abreviatura]] </td>
								</tr>
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="lineaproducto-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[lineaproducto.id == 0 ? "Agregar Linea de producto": "Editar lineaproducto"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-lineaproducto" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="lineaproducto_id" ng-model="lineaproducto.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated"
									  		 placeholder="Ej: computadora, teclado." required ng-model="lineaproducto.nombre">
										</div>
			        			</div>
			        			<div class="form-group">
										<label class="col-lg-2 control-label">Abreviatura</label>
										<div class="col-lg-10">
											<input type="hidden" name="lineaproducto_id" ng-model="lineaproducto.id">
									  		<input type="text" name="abreviatura" class="form-control parsley-validated"
									  		 placeholder="Ej: 1 pza. pzas." required ng-model="lineaproducto.abreviatura">
										</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-lineaproducto" value="Guardar" ng-click="guardarLineaproducto()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
<script type="text/javascript" src="{{asset('js/parsley.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
    $('#agregar-lineaproducto').click(function(){
      $('#lineasproducto-form').modal('show');
    });
    $('#registrar-lineaproducto').click(function(e){
      e.preventDefault();
    });
    $('#form-lineaproducto').parsley('validate');
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('lineasproducto_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('lineasproducto_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $scope.lineasproducto = [];
    $scope.lineaproducto = {
      id:0,
      nombre:'',
      abreviatura:''
    };
    //Funcion para traer registros de lineas de producto
    $scope.obtenerLineasproducto = function(){
      $http.get('lineasproducto/listado').success(function(response){
        $scope.lineasproducto = response;
      });
    }
    $scope.obtenerLineasproducto();
    $scope.nuevaLineaproducto = function(){
    	var lineaproducto = {
      id:0,
      nombre:'',
      abreviatura:''
    };
	    $scope.fijarLineaproducto(lineaproducto);
    }
    $scope.fijarLineaproducto = function(lineaproducto){
    	$scope.lineaproducto.id = lineaproducto.id;
    	$scope.lineaproducto.nombre = lineaproducto.nombre;
    	$scope.lineaproducto.abreviatura = lineaproducto.abreviatura;
    	$('#lineaproducto-form').modal('show');
    }
    $scope.guardarLineaproducto = function(){
    	$('#form-lineaproducto').parsley('validate');
    	return;
      $http({
        url: 'lineasproducto/guardar',
        method: 'post',
        data: $scope.lineaproducto
      }).success(function(response){
        $scope.obtenerLineasproducto();
        if(response == "ok")
        	$('#lineaproducto-form').modal('hide');
        else
        	alert("Error al guardar");
      });
    } 
    $scope.eliminarLineaproducto = function(lineaproducto){
    	if(confirm("¿Estás seguro de querer eliminar la linea de producto?")){
	    		$scope.lineaproducto.id = lineaproducto.id;
	    	$http({
	    		url:'lineasproducto/eliminar',
	    		method: 'post',
	    		data: $scope.lineaproducto
	    	}).then(function(response){
	    		if(response.data == 'ok'){
	    			alert('Se elimino correctamente');
	    			$scope.obtenerLineasproducto();
	    		}else {
	    			alert('Error al eliminar registro');
	    		}
	    	},function(response){console.log(response.data)});
    	}
    	
    }
  }]);
</script>
@endsection