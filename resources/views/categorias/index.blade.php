@extends('layouts.categorias')
@section('content')
<div ng-app="categorias_app">
  	<div ng-controller="categorias_controlador">
		<section class="content-header">
		  <a ng-click="nuevaCategoria()" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Categoria</a>
		  <h1>
		    Categorias
		    <small>del producto</small>
		  </h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th></th>
									<th>Nombre</th>
								</tr>
					        </thead>
								<tr ng-repeat="categoria in categorias">
									<td><a class="btn btn-circle btn-primary" title="Editar categoria" ng-click="fijarCategoria(categoria)"><i class="fa fa-pencil"></i></a></td>
									<td><a class="btn btn-circle btn-primary" title="Eliminar categoria"  ng-click="eliminarCategoria(categoria)"><i class="fa fa-trash"></i></a></td>
									<td>[[categoria.nombre]] </td>
									
								</tr>
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="categoria-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[categoria.id == 0 ? "Agregar Categoria": "Editar categoria"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-categoria" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="categoria_id" ng-model="categoria.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated"
									  		 placeholder="Ej: Dispositivos" required ng-model="categoria.nombre">
										</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-categoria" value="Guardar" ng-click="guardarCategoria()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
    $('#agregar-categoria').click(function(){
      $('#categoria-form').modal('show');
    });
    $('#registrar-categoria').click(function(e){
      e.preventDefault();
    });
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('categorias_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('categorias_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $scope.categorias = [];
    $scope.categoria = {
      id:0,
      nombre:''
    };
    //Funcion para traer registros de categorias
    $scope.obtenerCategorias = function(){
      $http.get('categorias/listado').success(function(response){
        $scope.categorias = response;
      });
    }
    $scope.obtenerCategorias();
    $scope.nuevaCategoria = function(){
    	var categoria = {
      id:0,
      nombre:''
    };
	    $scope.fijarCategoria(categoria);
    }
    $scope.fijarCategoria = function(categoria){
    	$scope.categoria.id = categoria.id;
    	$scope.categoria.nombre = categoria.nombre;
    	$('#categoria-form').modal('show');
    }
    $scope.guardarCategoria = function(){
     
      $http({
        url: 'categorias/guardar',
        method: 'post',
        data: $scope.categoria
      }).success(function(response){
        $scope.obtenerCategorias();
        if(response == "ok")
        	$('#categoria-form').modal('hide');
        else
        	alert("Error al guardar");
      });
    } 
    $scope.eliminarCategoria = function(categoria){
    	if(confirm("¿Estás seguro de querer eliminar la categoria?")){
	    		$scope.categoria.id = categoria.id;
	    	$http({
	    		url:'categorias/eliminar',
	    		method: 'post',
	    		data: $scope.categoria
	    	}).then(function(response){
	    		if(response.data == 'ok'){
	    			alert('Se elimino correctamente');
	    			$scope.obtenerCategorias();
	    		}else {
	    			alert('Error al eliminar registro');
	    		}
	    	},function(response){console.log(response.data)});
    	}
    	
    }
  }]);
</script>
@endsection