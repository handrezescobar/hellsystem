@extends('layouts.puntoventa')
@section('content')
<div ng-app="puntoventa_app">
  <div ng-controller="puntoventa_controlador">
    <section class="content-header">
      <h1>
        Ventas
        <small> Punto de venta</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-md-12 text-right">
              <a href="#" class="btn btn-primary" id="agregar-cliente"><i class="fa fa-plus"></i> Seleccionar Cliente</a>
              <a href="#" class="btn btn-primary" id="agregar-producto"><i class="fa fa-plus"></i> Agregar Producto</a>
              <a href="#" class="btn btn-danger" ng-click="cancelarVenta()"><i class="fa fa-times"></i> Cancelar Venta</a>
            </div>
          </div>
          <h2 class="page-header">
            <i class="fa fa-suitcase"></i> [[venta.cliente.nombre?(venta.cliente.nombre+' '+venta.cliente.apellido):'Selecciona cliente']]
            <small class="pull-right">Fecha: <?php echo date('d/m/Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-6 invoice-col" ng-if="venta.cliente.nombre">
            <h3>Datos de cliente</h3>
            <address>
              <b>Nombre: </b>[[venta.cliente.nombre]] [[venta.cliente.apellido]]<br>
              <b>R.F.C.: </b>[[venta.cliente.rfc]]<br>
              <b>Teléfono: </b>[[venta.cliente.telefono]]<br>
              <b>Correo: </b>[[venta.cliente.email]]<br>
              <b>Dirección: </b>[[venta.cliente.direccion]], C.P. [[venta.cliente.codigopostal]], [[venta.cliente.ciudad]], [[venta.cliente.estado]]<br>
            </address>            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Código</th>
              <th>Descripción</th>
              <th>Costo Unitario</th>
              <th>Cantidad</th>
              <th>Importe</th>
              <th></th>
              <th></th>
            </tr>
            </thead>
            <tbody>
              <tr ng-repeat="producto in venta.productos track by $index">
              	<td>[[producto.codigo]]</td>
              	<td>[[producto.descripcion]]</td>
              	<td>$[[producto.precioventa | number:2]]</td>
              	<td>[[producto.cantidad]]</td>
              	<td>$[[producto.importe | number:2]]</td>
                <td><a class="btn btn-primary btn-circle btn-sm" ng-click="editarProducto(producto, $index)"><i class="fa fa-pencil"></i></a></td>
              	<td><a class="btn btn-warning btn-circle btn-sm" ng-click="eliminarProducto($index)"><i class="fa fa-times"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Fecha de expedición <?php echo date('d/m/Y');?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Importe:</th>
                <td>$[[venta.importe | number:2]]</td>
              </tr>
              <tr>
                <th>Descuento:</th>
                <td>
                <div class="input-group col-md-4">
                  <input type="text" class="form-control input-sm" ng-model="venta.descuentop" ng-keyup="totales()">
                  <span class="input-group-addon">%</span>
                  </div> - $[[venta.descuentod | number:2]]
                </td>
              </tr>
              <tr>
                <th>Subtotal:</th>
                <td>$[[venta.subtotal | number:2]]</td>
              </tr>
              <tr>
                <th>IVA (16%):</th>
                <td>$[[venta.iva | number:2]]</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>$[[venta.total | number:2]]</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <!--<a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>-->
          <a href="#" class="btn btn-success pull-right" ng-click="guardarVenta()"><i class="fa fa-credit-card"></i> Guardar Venta
          </a>
          <!--<button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>-->
        </div>
      </div>
    </section>
    <div class="modal fade" id="cliente-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Selecciona Cliente</h4>
          </div>
            <form class="form-horizontal" id="form-selecciona-cliente">
              <div class="modal-body">
                  <fieldset>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Cliente</label>
                        <div class="col-lg-10">
                          <select name="cliente_id" class="form-control select-picker" ng-model="venta.cliente_id" data-live-search="true" data-live-search-placeholder="Selecciona un cliente" data-live-search-style="begins" title="Selecciona un cliente">
                            <option value="">Selecciona un cliente</option>
                            <option ng-repeat="cliente in clientes" data-cliente="[[cliente]]" value="[[cliente.id]]">[[cliente.nombre]] [[cliente.apellido]]</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Agregar Cliente" name="agrega_cliente" ng-click="seleccionarCliente()">
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="producto-modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Selecciona Producto</h4>
          </div>
            <form class="form-horizontal" id="form-agrega-producto" method="post" action="puntoventa.php">
              <div class="modal-body">
                  <div class="col-md-12">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Descripción</th>
                          <th>Código</th>
                          <th>Marca</th>
                          <th>Categoría</th>
                          <th>Línea de producto</th>
                          <th>Costo</th>
                          <th>Existencia</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="producto in productos" ng-class="producto.existencia < 1?'disabled danger':''">
                          <td>[[producto.descripcion]]</td>
                          <td>[[producto.codigo]]</td>
                          <td>[[producto.marca]]</td>
                          <td>[[producto.categoria]]</td>
                          <td>[[producto.lineaproducto]]</td>
                          <td>$[[producto.precioventa | number:2]]</td>
                          <td>[[producto.existencia]]</td>
                          <td><a ng-show="producto.existencia > 0" href="#" class="btn btn-circle btn-primary" ng-click="seleccionaProducto(producto)"><i class="fa fa-plus"></i></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="aproducto-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Cantidad de producto [[aproducto.descripcion]]</h4>
          </div>
            <form class="form-horizontal" id="form-selecciona-cliente">
              <div class="modal-body">
                  <fieldset>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Cantidad</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" ng-model="aproducto.cantidad" placeholder="0" ng-keyup="calcularImporte()">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Existencia</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="aproducto.existencia">
                          <h4>[[aproducto.existencia]]</h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Precio</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="aproducto.precioventa">
                          <h4>$<span>[[aproducto.precioventa | number:2]]</span></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Importe</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="aproducto.importe">
                          <h4>$<span>[[aproducto.importe | number:2]]</span></h4>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Agregar Producto" name="agrega_cliente" ng-click="agregarProducto()">
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="eaproducto-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Editar cantidad de producto [[aproducto.descripcion]]</h4>
          </div>
            <form class="form-horizontal" id="form-selecciona-cliente">
              <div class="modal-body">
                  <fieldset>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Cantidad</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" ng-model="eaproducto.cantidad" placeholder="0" ng-keyup="calcularImporteE()">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Existencia</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="eaproducto.existencia">
                          <h4>[[eaproducto.existencia]]</h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Precio</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="eaproducto.precioventa">
                          <h4>$<span>[[eaproducto.precioventa | number:2]]</span></h4>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Importe</label>
                        <div class="col-lg-10">
                          <input type="hidden" ng-model="eaproducto.importe">
                          <h4>$<span>[[eaproducto.importe | number:2]]</span></h4>
                        </div>
                      </div>
                    </div>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Guardar Producto" ng-click="guardarEdicionProducto()">
              </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="ver-venta-modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
            <h4 class="modal-title">Resumen de venta <a href="{{ url('/ventas/imprimir/') }}/[[ventav.id]]" class="btn btn-success pull-right" target="_blank"><i class="fa fa-print"></i> Imprimir factura</a></h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <h3>Datos de cliente</h3>
                  <address>
                    <b>Nombre: </b>[[ventav.cliente_nombre]]<br>
                    <b>R.F.C.: </b>[[ventav.cliente_rfc]]<br>
                    <b>Teléfono: </b>[[ventav.cliente_telefono]]<br>
                    <b>Correo: </b>[[ventav.cliente_email]]<br>
                    <b>Dirección: </b>[[ventav.cliente_direccion]], C.P. [[ventav.cliente_codigopostal]], [[ventav.cliente_ciudad]], [[ventav.cliente_estado]]<br>
                  </address>
                </div>
                <div class="col-md-4">
                  <h3>Datos de Usuario</h3>
                  <address>
                    <b>Nombre: </b>[[ventav.usuario]]<br>
                    <b>Email: </b>[[ventav.usuario_email]]<br>
                  </address>
                </div>
                <div class="col-md-4" class="text-right">
                  <h3 class="text-right">Totales</h3>
                  <address class="text-right">
                    <h4>
                    <b>Importe: </b>$[[ventav.importe | number:2]]<br><br>
                    <b>Descuento: </b>[[ventav.descuentop]]% - $[[ventav.descuentod | number:2]]<br><br>
                    <b>Subtotal: </b>$[[ventav.subtotal | number:2]]<br><br>
                    <b>I.V.A.: </b>$[[ventav.iva | number:2]]<br><br>
                    <b>Total: </b>$[[ventav.total | number:2]]<br></h4>
                  </address>
                </div>
              </div>
              <div class="row">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Descripción</th>
                      <th>Costo Unitario</th>
                      <th>Cantidad</th>
                      <th>Importe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="producto in ventav.productos track by $index">
                      <td>[[producto.codigo]]</td>
                      <td>[[producto.descripcion]]</td>
                      <td>$[[producto.precioventa | number:2]]</td>
                      <td>[[producto.cantidad]]</td>
                      <td>$[[producto.importe | number:2]]</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
  </div>
</div>
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/format-number/jquery.format-number.plugin.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#agregar-cliente').click(function(){
      $('#cliente-modal').modal('show');
    });
    $('#agregar-producto').click(function(){
      $('#producto-modal').modal('show');
    });
    //$('.select-picker').selectpicker();
    /*$('#agregar-producto').click(function(){
      $('#producto-modal').modal('show');
    });*/
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('puntoventa_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('puntoventa_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $scope.venta = {
      cliente_id:'',
      cliente: {},
      productos:[],
      importe:0.0,
      descuentop:0,
      descuentod:0.0,
      subtotal:0.0,
      iva:0.0,
      total:0.0
    };
    $scope.clientes = [];
    $scope.productos = [];
    $http.get('/clientes/listado').then(
      function(response){
        console.log(response.data);
        $scope.clientes = response.data;
      },
      function(response){
        console.log(response.data);
      });
    $scope.obtenerProductos = function(){
      $http.get('/ventas/productos-venta').then(
        function(response){
          console.log(response.data);
          $scope.productos = response.data;
        },
        function(response){
          console.log(response.data);
        });
    }
    $scope.obtenerProductos();
    $scope.seleccionarCliente = function(){
      angular.forEach($scope.clientes, function(cliente, index){
        if($scope.venta.cliente_id == cliente.id){
          $scope.cancelarVenta();
          $scope.venta.cliente = cliente;
          $scope.venta.cliente_id = cliente.id
          return false;
        } 
      });
      console.log($scope.venta);
      $('#cliente-modal').modal('hide');
    }
    $scope.aproducto;
    $scope.seleccionaProducto = function(producto){
      $scope.aproducto = producto;
      $scope.aproducto.cantidad = '';
      $scope.aproducto.importe = 0;
      console.log($scope.aproducto);
      $('#aproducto-modal').modal('show');
    }
    $scope.calcularImporte = function(){
      if($scope.aproducto.cantidad > $scope.aproducto.existencia){
        alert('La cantidad de producto ingresada no está disponible en almacén');
        $scope.aproducto.cantidad = '';
        //return;
      }
      $scope.aproducto.importe = $scope.aproducto.cantidad * $scope.aproducto.precioventa;
      console.log($scope.aproducto.importe);
    }
    $scope.calcularImporteE = function(){
      if($scope.eaproducto.cantidad > $scope.eaproducto.existencia){
        alert('La cantidad de producto ingresada no está disponible en almacén');
        $scope.eaproducto.cantidad = '';
        //return;
      }
      $scope.eaproducto.importe = $scope.eaproducto.cantidad * $scope.eaproducto.precioventa;
      console.log($scope.eaproducto.importe);
    }
    $scope.agregarProducto = function(){
      $scope.venta.productos.push($scope.aproducto);
      $scope.totales();
      $scope.productos[$scope.productos.indexOf($scope.aproducto)].existencia -= $scope.aproducto.cantidad;
      $('#aproducto-modal').modal('hide'); 
      $('#producto-modal').modal('hide');
    }
    $scope.eaproducto;
    $scope.editarProducto = function(producto, index){
      $scope.eaproducto = producto;
      $scope.eaproducto.index = index;
      $scope.eaproducto.cantidada = producto.cantidad;
      $('#eaproducto-modal').modal('show');
    }
    $scope.guardarEdicionProducto = function(){
      console.log($scope.eaproducto);
      //return;
      $scope.venta.productos[$scope.eaproducto.index] = $scope.eaproducto;
      $scope.totales();
      $scope.productos[$scope.productos.indexOf($scope.eaproducto)].existencia += parseInt($scope.eaproducto.cantidada);
      $scope.productos[$scope.productos.indexOf($scope.eaproducto)].existencia -= parseInt($scope.eaproducto.cantidad);
      $('#eaproducto-modal').modal('hide'); 
    }
    $scope.eliminarProducto = function(index){
      if(confirm("¿Estás seguro de querer eliminar el producto de la venta?")){
        var producto = $scope.venta.productos[index];
        $scope.productos[$scope.productos.indexOf(producto)].existencia += parseInt(producto.cantidad);
        $scope.venta.productos.splice(index,1);
      }
      $scope.totales();
    }
    $scope.totales = function(){
      $scope.venta.importe = 0;
      angular.forEach($scope.venta.productos, function(producto, index){
        $scope.venta.importe += producto.importe;
      });
      $scope.venta.descuentod = ($scope.venta.importe*$scope.venta.descuentop)/100;
      $scope.venta.subtotal = $scope.venta.importe - $scope.venta.descuentod;
      $scope.venta.iva = $scope.venta.subtotal*.16;
      $scope.venta.total = $scope.venta.subtotal + $scope.venta.iva;
      console.log($scope.venta);
    }
    $scope.cancelarVenta = function(){
      $scope.venta.cliente_id = '';
      $scope.venta.cliente = {};
      $scope.venta.productos = [];
      $scope.venta.importe = 0.0;
      $scope.venta.descuentop = 0;
      $scope.venta.descuentod = 0.0;
      $scope.venta.subtotal = 0.0;
      $scope.venta.iva = 0.0;
      $scope.venta.total = 0.0;
      $scope.obtenerProductos();
    }
    $scope.guardarVenta = function(){
      if(!$scope.venta.cliente.nombre){
        alert("Comienza seleccionando un cliente");
        return;
      }
      if($scope.venta.productos.length <= 0){
        alert("Agrega por lo menos un producto a la venta");
        return;
      }
      $scope.ventav;
      $http.post('/ventas/guardar-venta',$scope.venta).then(
        function(response){
          console.log(response.data);
          if(response.data != 'error'){
            var venta_id = response.data;
            alert('Se guardo con éxito la venta');
            $http.get('/ventas/obtener-venta/'+venta_id).then(
              function(response){
                console.log(response.data);
                $scope.ventav = response.data;
                $('#ver-venta-modal').modal('show');
              });
            $scope.cancelarVenta();
          }
        },function(response){
          console.log(response.data);
        });
    }
  }]);
</script>
@endsection