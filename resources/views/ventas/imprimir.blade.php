@extends('ventas.imprimir_template')
@section('content')
<div class="wrapper">        
  <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
            <img src="{{asset('img/logo_hellsystem.png')}}" width="150" class="pull-right">
            <h1>@Hell_Systems</h1><br><br><br>
              <h2 class="page-header">
                <i class="fa fa-suitcase"></i> Cliente: {{$venta['cliente_nombre']}}
                <small class="pull-right">Fecha: {{$venta['fecha']}}</small>
              </h2>
            </div>
            <!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                  <h3>Datos de cliente</h3>
                  <address>
                    <b>Nombre: </b>{{$venta['cliente_nombre']}}<br>
                    <b>R.F.C.: </b>{{$venta['cliente_rfc']}}<br>
                    <b>Teléfono: </b>{{$venta['cliente_telefono']}}<br>
                    <b>Correo: </b>{{$venta['cliente_email']}}<br>
                    <b>Dirección: </b>{{$venta['cliente_direccion']}}, C.P. {{$venta['cliente_codigopostal']}}, {{$venta['ciudad']}}, {{$venta['estado']}}<br>
                  </address>
                </div>
                <div class="col-sm-4 invoice-col">
                  <h3>Datos de Usuario</h3>
                  <address>
                    <b>Nombre: </b>{{$venta['usuario']}}<br>
                    <b>Email: </b>{{$venta['usuario_email']}}<br>
                  </address>
                </div>
                <div class="col-sm-4 invoice-col" class="text-right">
                  <h3 class="text-right">Totales</h3>
                  <address class="text-right">
                    <h4>
                    <b>Importe: </b>$<span class="money">{{$venta['importe']}}</span><br><br>
                    <b>Descuento: </b>{{$venta['descuentop']}}% - $<span class="money">{{$venta['descuentod']}}</span><br><br>
                    <b>Subtotal: </b>$<span class="money">{{$venta['subtotal']}}</span><br><br>
                    <b>I.V.A.: </b>$<span class="money">{{$venta['iva']}}</span><br><br>
                    <b>Total: </b>$<span class="money">{{$venta['total']}}</span><br></h4>
                  </address>
                </div>
              </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <h5>Productos:</h5>
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Código</th>
                  <th>Descripción</th>
                  <th>Costo Unitario</th>
                  <th>Cantidad</th>
                  <th>Importe</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($productos as $index => $producto)
                    <tr>
                      <td>{{$producto['codigo']}}</td>
                      <td>{{$producto['descripcion']}}</td>
                      <td>$<span class="money">{{$producto['precioventa']}}</span></td>
                      <td>{{$producto['cantidad']}}</td>
                      <td>$<span class="money">{{$producto['importe']}}</span></td>
                    </tr>               
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
  </section>
   <div class="clearfix"></div>
</div><!-- /.content-wrapper -->
<footer class="main-footer">
</footer>
@endsection