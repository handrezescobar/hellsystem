@extends('layouts.ventas')
@section('content')
<div ng-app="ventas_app">
  <div ng-controller="ventas_controlador">
    <section class="content-header">
      <a href="{{ url('ventas/punto-venta') }}" class="btn btn-primary pull-right" style="margin-left:5px;"><i class="fa fa-plus"></i> Agregar Venta</a>
      <a href="{{ url('ventas/reporte') }}" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> Exportar Reporte</a>
      <h1>
        Resumen
        <small> Resumen de ventas</small>
      </h1>
    </section>
    <section class="content">
      <div class="col-lg-12">
        <div class="row">
          <div class="panel">
            <div class="panel-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>Cliente</th>
                    <th>Importe</th>
                    <th>Descuento</th>
                    <th>Subtotal</th>
                    <th>I.V.A.</th>
                    <th>Total</th>
                    <th>Usuario</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="venta in ventas">
                    <td>[[venta.cliente]]</td>
                    <td>$[[venta.importe | number:2]]</td>
                    <td>[[venta.descuentop]]% - $[[venta.descuentod | number:2]]</td>
                    <td>$[[venta.subtotal | number:2]]</td>
                    <td>$[[venta.iva | number:2]]</tdv>
                    <td>$[[venta.total | number:2]]</td>
                    <td>[[venta.usuario]]</td>
                    <td><a class="btn btn-primary btn-circle" ng-click="verVenta(venta)"><i class="fa fa-search"></i></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="modal fade" id="ver-venta-modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
            <h4 class="modal-title">Resumen de venta <a href="{{ url('/ventas/imprimir/') }}/[[venta.id]]" class="btn btn-success pull-right" target="_blank"><i class="fa fa-print"></i> Imprimir factura</a></h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4">
                  <h3>Datos de cliente</h3>
                  <address>
                    <b>Nombre: </b>[[venta.cliente_nombre]]<br>
                    <b>R.F.C.: </b>[[venta.cliente_rfc]]<br>
                    <b>Teléfono: </b>[[venta.cliente_telefono]]<br>
                    <b>Correo: </b>[[venta.cliente_email]]<br>
                    <b>Dirección: </b>[[venta.cliente_direccion]], C.P. [[venta.cliente_codigopostal]], [[venta.cliente_ciudad]], [[venta.cliente_estado]]<br>
                  </address>
                </div>
                <div class="col-md-4">
                  <h3>Datos de Usuario</h3>
                  <address>
                    <b>Nombre: </b>[[venta.usuario]]<br>
                    <b>Email: </b>[[venta.usuario_email]]<br>
                  </address>
                </div>
                <div class="col-md-4" class="text-right">
                  <h3 class="text-right">Totales</h3>
                  <address class="text-right">
                    <h4>
                    <b>Importe: </b>$[[venta.importe | number:2]]<br><br>
                    <b>Descuento: </b>[[venta.descuentop]]% - $[[venta.descuentod | number:2]]<br><br>
                    <b>Subtotal: </b>$[[venta.subtotal | number:2]]<br><br>
                    <b>I.V.A.: </b>$[[venta.iva | number:2]]<br><br>
                    <b>Total: </b>$[[venta.total | number:2]]<br></h4>
                  </address>
                </div>
              </div>
              <div class="row">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Código</th>
                      <th>Descripción</th>
                      <th>Costo Unitario</th>
                      <th>Cantidad</th>
                      <th>Importe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="producto in venta.productos track by $index">
                      <td>[[producto.codigo]]</td>
                      <td>[[producto.descripcion]]</td>
                      <td>$[[producto.precioventa | number:2]]</td>
                      <td>[[producto.cantidad]]</td>
                      <td>$[[producto.importe | number:2]]</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    </div>
  </div>
</div>
<script src="{{asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/format-number/jquery.format-number.plugin.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('ventas_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('ventas_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
    $ventas = [];
    $http.get('/ventas/obtener-ventas').then(function(response){console.log(response.data);$scope.ventas = response.data},function(response){console.log(response.data);});
    $scope.venta;
    $scope.verVenta = function(venta){
      $http.get('/ventas/obtener-venta/'+venta.id).then(
        function(response){
          console.log(response.data);
          $scope.venta = response.data;
          $('#ver-venta-modal').modal('show');
        });

    }

  }]);
</script>
@endsection