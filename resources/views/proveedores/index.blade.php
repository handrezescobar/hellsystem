@extends('layouts.proveedores')
@section('content')
<div ng-app="proveedores_app">
  	<div ng-controller="proveedores_controlador">
		<section class="content-header">
		  <a ng-click="nuevoProveedor()" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Proveedor</a>
		  <h1>
		    Preveedores
		    <small></small>
		  </h1>
		</section>
		<section class="content">
			<div class="col-lg-12">
			   	<div class="row">
				  	<div class="panel">
					    <div class="panel-body">
					      <table class="table table-striped table-hover ">
					        <thead>
								<tr>
									<th></th>
									<th>Nombre</th>
									<th>Representante</th>
									<th>RFC</th>
									<th>Teléfono</th>
									<th>Correo</th>
									<th>Dirección</th>
								</tr>
					        </thead>
								<tr ng-repeat="proveedor in proveedores">
									<td><a class="btn btn-circle btn-primary" title="Editar proveedor" ng-click="fijarProveedor(proveedor)"><i class="fa fa-pencil"></i></a></td>
									<td>[[proveedor.nombre]]</td>
									<td>[[proveedor.representante]]</td>
									<td>[[proveedor.rfc]]</td>
									<td>[[proveedor.telefono]]</td>
									<td>[[proveedor.email]]</td>
									<td>[[proveedor.direccion]], [[proveedor.codigopostal]], [[proveedor.ciudad]], [[proveedor.estado]]</td> 
								</tr>
					      </table>
					    </div>
				  	</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="proveedor-form">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title">[[proveedor.id == 0 ? "Agregar proveedor": "Editar proveedor"]]</h4>
					</div>
				  	<form class="form-horizontal" id="form-proveedor" method="post" action="#">
				  		<div class="modal-body">
				      		<fieldset>
				        		<div class="col-md-12">
									<div class="form-group">
										<label class="col-lg-2 control-label">Nombre</label>
										<div class="col-lg-10">
											<input type="hidden" name="proveedor_id" ng-model="proveedor.id">
									  		<input type="text" name="nombre" class="form-control parsley-validated" placeholder="Ej: Juan" required ng-model="proveedor.nombre">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Representante</label>
										<div class="col-lg-10">
									  		<input type="text" name="apellido" class="form-control parsley-validated" placeholder="Ej: López" required ng-model="proveedor.representante">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">RFC</label>
										<div class="col-lg-10">
									  		<input type="text" name="apellido" class="form-control parsley-validated" placeholder="Ej: JUOP21212121" required ng-model="proveedor.rfc">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Correo Electrónico</label>
										<div class="col-lg-10">
									  		<input type="text" name="email" class="form-control parsley-validated" placeholder="Ej: juan@mail.com" required ng-model="proveedor.email">
										</div>
									</div>
									<div class="form-group" ng-show="proveedor.id == 0">
										<label class="col-lg-2 control-label">Teléfono</label>
										<div class="col-lg-10">
									  		<input type="text" name="telefono" class="form-control parsley-validated" required ng-model="proveedor.telefono">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Dirección</label>
										<div class="col-lg-10">
									  		<input type="text" name="direccion" class="form-control parsley-validated" required ng-model="proveedor.direccion">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Código Postal</label>
										<div class="col-lg-10">
									  		<input type="text" name="codigopostal" class="form-control parsley-validated" required ng-model="proveedor.codigopostal">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Estado</label>
										<div class="col-lg-10">
									  		<select class="form-control" ng-model="proveedor.estado_id" ng-change="obtenerCiudades()" id="estado-id">
									  			<option ng-repeat="estado in estados" value="[[estado.id]]">[[estado.nombre]]</option>
									  		</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Ciudad</label>
										<div class="col-lg-10">
									  		<select class="form-control" ng-model="proveedor.ciudad_id" id="ciudad-id">
									  			<option ng-repeat="ciudad in ciudades" value="[[ciudad.id]]">[[ciudad.nombre]]</option>
									  		</select>
										</div>
									</div>
			        			</div>
			      			</fieldset>
				  		</div>
					  	<div class="modal-footer">
						    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						    <input type="submit" class="btn btn-primary" id="registrar-proveedor" value="Guardar" ng-click="guardarProveedor()">
					  	</div>
			  		</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.select2').select2();
    $('#agregar-proveedor').click(function(){
      $('#proveedor-form').modal('show');
    });
    $('#registrar-proveedor').click(function(e){
      e.preventDefault();
    });
  });
</script>
<script type="text/javascript">
  var AppAng = angular.module('proveedores_app',[],function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
  });
  AppAng.controller('proveedores_controlador',['$scope','$http','$filter', function($scope, $http, $filter){
  	$scope.estados = [];
  	$scope.ciudades = [];
  	$http.get('catalogo/estados').then(function(response){$scope.estados = response.data},function(response){console.log(response.data)});
  	$scope.obtenerCiudades = function(){
  		angular.forEach($scope.estados, function(estado, index){
  			if(estado.id == $scope.proveedor.estado_id){
  				$scope.ciudades = estado.ciudades;
  				return;
  			}
  		});
  	}
    $scope.proveedores = [];
    $scope.proveedor = {
      id:0,
      nombre:'',
      representante:'',
      email:'',
      telefono:'',
      rfc:'',
      direccion:'',
      codigopostal:'',
      estado_id:'',
      ciudad_id:''
    };
    //Función para traer registro de proveedores
    $scope.obtenerProveedores = function(){
      $http.get('proveedores/listado').success(function(response){
        $scope.proveedores = response;
      });
    }
    $scope.obtenerProveedores();
    $scope.nuevoProveedor = function(){
    	var proveedor = {
	      id:0,
	      nombre:'',
	      representante:'',
	      email:'',
	      telefono:'',
	      rfc:'',
	      direccion:'',
	      codigopostal:'',
	      estado_id:'',
	      ciudad_id:''
	    };
	    $scope.fijarProveedor(proveedor);
    }
    $scope.fijarProveedor = function(proveedor){
    	$scope.proveedor.id = proveedor.id;
    	$scope.proveedor.nombre = proveedor.nombre;
    	$scope.proveedor.representante = proveedor.representante;
    	$scope.proveedor.email = proveedor.email;
    	$scope.proveedor.rfc = proveedor.rfc;
    	$scope.proveedor.telefono = proveedor.telefono;
    	$scope.proveedor.direccion = proveedor.direccion;
    	$scope.proveedor.codigopostal = proveedor.codigopostal;
    	$scope.proveedor.estado_id = proveedor.estado_id;
    	//$('#estado-id').change();
    	$scope.obtenerCiudades();
    	$scope.proveedor.ciudad_id = proveedor.ciudad_id;
    	setTimeout(function(){$('#ciudad-id').val(proveedor.ciudad_id);},300);
    	$('#proveedor-form').modal('show');
    }
    $scope.guardarProveedor = function(){
    	console.log($scope.proveedor);
      $http({
        url: 'proveedores/guardar',
        method: 'post',
        data: $scope.proveedor
      }).success(function(response){
        $scope.obtenerProveedores();
        if(response == "ok"){
        	$('#proveedor-form').modal('hide');
        }
        else{
        	alert("Error al guardar");
        }
      });
    }
  }]);
</script>
@endsection