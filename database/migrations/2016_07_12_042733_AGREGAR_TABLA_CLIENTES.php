<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AGREGARTABLACLIENTES extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('apellido',50);
            $table->string('email',100);
            $table->string('telefono',15);
            $table->string('rfc',20);
            $table->string('direccion',50);
            $table->integer('ciudad_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->string('codigopostal',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
