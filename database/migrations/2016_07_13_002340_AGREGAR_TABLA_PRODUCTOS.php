<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AGREGARTABLAPRODUCTOS extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->string('codigo',10);
            $table->integer('categoria_id')->unsigned();
            $table->integer('marca_id')->unsigned();
            $table->integer('lineaproducto_id')->unsigned();
            $table->boolean('estatus')->default(TRUE);
            $table->integer('existencia')->default(0);
            $table->timestamps();

            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('marca_id')->references('id')->on('marcas');
            $table->foreign('lineaproducto_id')->references('id')->on('lineasproducto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productos');
    }
}
