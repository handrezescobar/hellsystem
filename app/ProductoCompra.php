<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoCompra extends Model
{
    //
    protected $table = "productocompra";

    protected $fillable = ['producto_id','compra_id','cantidad','costounitario','importe'];
}
