<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    //
    protected $table = "ventas";

    protected $fillable = ['cliente_id','usuario_id','descporcentaje','descdinero','importe','subtotal','iva','total','fecha','estatus'];
}
