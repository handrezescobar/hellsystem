<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineaProducto extends Model
{
    //
    protected $table = 'lineasproducto';
    protected $fillable = ['nombre','abreviatura'];
}
