<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    //
    protected $table = "proveedores";

    protected $fillable = ['nombre','representante','rfc','telefono','email','direccion','ciudad_id','estado_id','codigopostal'];
}
