<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrecioProveedor extends Model
{
    //
    protected $table = "precioproveedor";

    protected $fillable = ['proveedor_id','producto_id','precio'];
}
