<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    //
    protected $table = "compras";

    protected $fillable = ['proveedor_id','usuario_id','subtotal','iva','total','fecha','estatus'];
}
