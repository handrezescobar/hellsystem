<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'], function () {
  Route::auth();
  Route::get('/', 'HomeController@index');

  Route::group(['prefix' => 'usuarios'], function(){
  	Route::get('/','UsuariosController@index');
  	Route::get('/listado', 'UsuariosController@listar');
  	Route::post('/guardar', 'UsuariosController@guardar');
  	Route::post('/restaurar-password', 'UsuariosController@restaurarPassword');
  });
  Route::group(['prefix' => 'clientes'], function(){
  	Route::get('/', 'ClientesController@index');
    Route::get('/listado', 'ClientesController@listado');
    Route::post('/guardar', 'ClientesController@guardar');
  });
  Route::group(['prefix'=> 'marcas'], function() {
    Route::get('/','MarcasController@index');
    Route::get('/listado', 'MarcasController@listado');
    Route::post('/guardar','MarcasController@guardar');
    Route::post('/eliminar','MarcasController@eliminar');
  });
  Route::group(['prefix' => 'categorias'], function(){
    Route::get('/','CategoriasController@index');
    Route::get('/listado', 'CategoriasController@listado');
    Route::post('/guardar', 'CategoriasController@guardar');
    Route::post('/eliminar', 'CategoriasController@eliminar');
  });
  
  Route::group(['prefix' => 'proveedores'], function(){
    Route::get('/','ProveedoresController@index');
    Route::get('/listado', 'ProveedoresController@listado');
    Route::post('/guardar', 'ProveedoresController@guardar');
  });
  Route::group(['prefix' => 'catalogo'],function(){
    Route::get('/estados', 'EstadosController@obtenerEstados');
  });
   Route::group(['prefix' => 'lineasproducto'], function(){
    Route::get('/','LineasProductosController@index');
     Route::get('/listado', 'LineasProductosController@listado');
    Route::post('/guardar', 'LineasProductosController@guardar');
    Route::post('/eliminar', 'LineasProductosController@eliminar');
  });
  Route::group(['prefix' => 'almacen'],function(){
    Route::get('/','AlmacenController@index');
    Route::get('/obtener-productos','AlmacenController@obtenerProductos');
    Route::post('/guardar-producto','AlmacenController@guardarProducto');
    Route::get('/obtener-costos/{producto_id}','AlmacenController@obtenerCostos');
    Route::post('/guardar-costo', 'AlmacenController@guardarCosto');
    Route::get('/eliminar-costo/{costo_id}', 'AlmacenController@eliminarCosto');
    Route::get('/reporte','AlmacenController@exportar');
  });
  Route::group(['prefix' => 'compras'], function(){
    Route::get('/punto-compra','ComprasController@puntoCompra');
    Route::get('/obtener-productos/{proveedor_id}', 'ComprasController@obtenerProductos');
    Route::post('guardar-compra','ComprasController@guardarCompra');
    Route::get('/resumen','ComprasController@resumen');
    Route::get('/obtener-compras','ComprasController@obtenerCompras');
    Route::get('/obtener-compra/{compra_id}','ComprasController@obtenerCompra');
    Route::get('/imprimir/{compra_id}','ComprasController@imprimir');
    Route::get('/reporte','ComprasController@reporte');
  });
  Route::group(['prefix' => 'ventas'], function(){
    Route::get('/punto-venta','VentasController@puntoCompra');
    Route::get('/productos-venta', 'VentasController@obtenerProductosVenta');
    Route::post('guardar-venta','VentasController@guardarVenta');
    Route::get('/resumen','VentasController@resumen');
    Route::get('/obtener-ventas','VentasController@obtenerVentas');
    Route::get('/obtener-venta/{venta_id}','VentasController@obtenerVenta');
    Route::get('/imprimir/{venta_id}','VentasController@imprimir');
    Route::get('/reporte','VentasController@reporte');
  });
});
