<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Compra;
use App\ProductoCompra;
use App\Proveedor;
use App\PrecioProveedor;
use App\Producto;
use DB;
use Illuminate\Support\Facades\Auth;
use PDO;

class ComprasController extends Controller
{
    public function puntoCompra(){
    	$fecha = date('Y-m-d');
    	return view('compras.puntocompra',['fecha' => $fecha]);
    }
    public function obtenerProductos($proveedor_id){
		$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.id marca_id, m.nombre marca, c.id categoria_id, c.nombre categoria, l.id lineaproducto_id, l.nombre lineaproducto, p.estatus, p.existencia, pp.precio costo, p.updated_at FROM productos p INNER JOIN marcas m ON p.marca_id = m.id INNER JOIN categorias c ON p.categoria_id = c.id INNER JOIN  lineasproducto l ON p.lineaproducto_id = l.id INNER JOIN precioproveedor pp ON pp.producto_id = p.id WHERE pp.proveedor_id = $proveedor_id");
		return $productos;

	}
	public function guardarCompra(Request $request){
		//dd($request->all());
		$data = $request->all();
		$user = Auth::user();
		$compra = new Compra();
		$compra->proveedor_id = $data['proveedor_id'];
		$compra->usuario_id = $user->id;
		$compra->subtotal = $data['subtotal'];
		$compra->iva = $data['iva'];
		$compra->total = $data['total'];
		$compra->fecha = date('Y-m-d H:i:s');
		$compra->estatus = TRUE;
		$compra->save();
		foreach ($data['productos'] as $index => $producto) {
			$pc = new ProductoCompra();
			$pc->producto_id = $producto['id'];
			$pc->compra_id = $compra->id;
			$pc->cantidad = $producto['cantidad'];
			$pc->costounitario = $producto['costo'];
			$pc->importe = $producto['importe'];
			$pc->save();
			$pr = Producto::find($producto['id']);
			$pr->existencia += $producto['cantidad'];
			$pr->save();
		}
		echo $compra->id;
	}
	public function resumen(){
		return view('compras.resumen');
	}
	public function obtenerCompras(){
		$compras = DB::select("SELECT c.id, c.proveedor_id, p.nombre proveedor, c.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, c.subtotal, c.iva, c.total, c.fecha FROM compras c INNER JOIN proveedores p ON c.proveedor_id = p.id INNER JOIN users u ON c.usuario_id = u.id");
		return $compras;
	}
	public function obtenerCompra($compra_id){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$compra = DB::select("SELECT c.id, c.proveedor_id, p.nombre proveedor_nombre, p.representante proveedor_representante, p.rfc proveedor_rfc, p.telefono proveedor_telefono, p.email proveedor_email, p.direccion proveedor_direccion, p.codigopostal proveedor_codigopostal,e.nombre estado, cd.nombre ciudad, c.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, u.email usuario_email, c.subtotal, c.iva, c.total, c.fecha FROM compras c INNER JOIN proveedores p ON c.proveedor_id = p.id INNER JOIN estados e ON p.estado_id = e.id INNER JOIN ciudades cd ON p.ciudad_id = cd.id INNER JOIN users u ON c.usuario_id = u.id WHERE c.id = $compra_id")[0];
		$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.nombre marca, c.nombre categoria, l.nombre linea, pc.costounitario costo, pc.cantidad, pc.importe FROM productos p INNER JOIN marcas m ON m.id = p.marca_id INNER JOIN categorias c ON c.id = p.categoria_id INNER JOIN lineasproducto l ON l.id = p.lineaproducto_id INNER JOIN productocompra pc ON p.id = pc.producto_id WHERE pc.compra_id = $compra_id");
		$compra['productos'] = $productos;
		return $compra;
	}
	public function imprimir($compra_id){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$compra = DB::select("SELECT c.id, c.proveedor_id, p.nombre proveedor_nombre, p.representante proveedor_representante, p.rfc proveedor_rfc, p.telefono proveedor_telefono, p.email proveedor_email, p.direccion proveedor_direccion, p.codigopostal proveedor_codigopostal,e.nombre estado, cd.nombre ciudad, c.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, u.email usuario_email, c.subtotal, c.iva, c.total, c.fecha FROM compras c INNER JOIN proveedores p ON c.proveedor_id = p.id INNER JOIN estados e ON p.estado_id = e.id INNER JOIN ciudades cd ON p.ciudad_id = cd.id INNER JOIN users u ON c.usuario_id = u.id WHERE c.id = $compra_id")[0];
		$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.nombre marca, c.nombre categoria, l.nombre linea, pc.costounitario costo, pc.cantidad, pc.importe FROM productos p INNER JOIN marcas m ON m.id = p.marca_id INNER JOIN categorias c ON c.id = p.categoria_id INNER JOIN lineasproducto l ON l.id = p.lineaproducto_id INNER JOIN productocompra pc ON p.id = pc.producto_id WHERE pc.compra_id = $compra_id");
		return view('compras.imprimir',['compra' => $compra, 'productos' => $productos]);
	}
	public function reporte(){
		header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=reporte_compras_".date('Y-m-d').".xls");
        DB::setFetchMode(PDO::FETCH_ASSOC);
		$compras = DB::select("SELECT c.id, c.proveedor_id, p.nombre proveedor, c.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, c.subtotal, c.iva, c.total, c.fecha FROM compras c INNER JOIN proveedores p ON c.proveedor_id = p.id INNER JOIN users u ON c.usuario_id = u.id");
		$render = '<head>
                    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
                    </head>
            <table border="1">
                <thead>
                  <tr>
                    <th>Proveedor</th>
                    <th>Subtotal</th>
                    <th>IVA</th>
                    <th>Total</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                  </tr>
                </thead>
                <tbody>';
        foreach ($compras as $index => $compra) {
            $render.= "<tr>";
            $render.= "<td>".$compra['proveedor']."</td>";
            $render.= "<td>".$compra['subtotal']."</td>";
            $render.= "<td>".$compra['iva']."</td>";
            $render.= "<td>".$compra['total']."</td>";
            $render.= "<td>".$compra['usuario']."</td>";
            $render.= "<td>".$compra['fecha']."</td>";
            $render.= "</tr>";
        }
        $render.= "</tbody></table>";
        echo $render;
	}
}