<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Producto;
use App\PrecioProveedor;
use DB;
use PDO;

class AlmacenController extends Controller
{
    public function index(){
    	return view('almacen.index');
    }
    public function obtenerProductos(){
    	$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.id marca_id, m.nombre marca, c.id categoria_id, c.nombre categoria, l.id lineaproducto_id, l.nombre lineaproducto, p.estatus, p.existencia, p.updated_at FROM productos p INNER JOIN marcas m ON p.marca_id = m.id INNER JOIN categorias c ON p.categoria_id = c.id INNER JOIN  lineasproducto l ON p.lineaproducto_id = l.id");
    	return $productos;
    }
    public function guardarProducto(Request $request){
    	$data = $request->all();
    	if($data['id'] == 0){
    		$producto = new Producto();
	    	$producto->descripcion = $data['descripcion'];
	    	$producto->codigo = $data['codigo'];
	    	$producto->marca_id = $data['marca_id'];
	    	$producto->categoria_id = $data['categoria_id'];
	    	$producto->lineaproducto_id = $data['lineaproducto_id'];
	    	$producto->estatus = isset($data['estatus'])==true?1:0;
	    	$producto->existencia = 0;
	    	$producto->save();
    	} else {
    		$producto = Producto::find($data['id']);
	    	$producto->descripcion = $data['descripcion'];
	    	$producto->codigo = $data['codigo'];
	    	$producto->marca_id = $data['marca_id'];
	    	$producto->categoria_id = $data['categoria_id'];
	    	$producto->lineaproducto_id = $data['lineaproducto_id'];
	    	$producto->estatus = isset($data['estatus'])==true?1:0;
	    	$producto->save();
    	}
    	echo 'ok';
    }
    public function obtenerCostos($producto_id){
    	$costos = DB::select("SELECT c.id, p.id proveedor_id, p.nombre proveedor, c.precio, c.producto_id FROM precioproveedor c INNER JOIN proveedores p ON c.proveedor_id = p.id WHERE c.producto_id = $producto_id");
    	return $costos;
    }
    public function guardarCosto(Request $request){
    	$data = $request->all();
    	if($data['id'] == 0){
    		$costo = new PrecioProveedor();
	    	$costo->producto_id = $data['producto_id'];
	    	$costo->proveedor_id = $data['proveedor_id'];
	    	$costo->precio = $data['costo'];
	    	$costo->save();
    	} else {
    		$costo = PrecioProveedor::find($data['id']);
	    	$costo->producto_id = $data['producto_id'];
	    	$costo->proveedor_id = $data['proveedor_id'];
	    	$costo->precio = $data['costo'];
	    	$costo->save();
    	}
    	echo "ok";
    }
    public function eliminarCosto($costo_id){
    	$costo = PrecioProveedor::find($costo_id);
    	$costo->delete();
    	echo "ok";
    }
    public function exportar(){
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=reporte_inventario_".date('Y-m-d').".xls");
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.id marca_id, m.nombre marca, c.id categoria_id, c.nombre categoria, l.id lineaproducto_id, l.nombre lineaproducto, p.estatus, p.existencia, p.created_at FROM productos p INNER JOIN marcas m ON p.marca_id = m.id INNER JOIN categorias c ON p.categoria_id = c.id INNER JOIN  lineasproducto l ON p.lineaproducto_id = l.id");
        $render = '<head>
                    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
                    </head>
            <table border="1">
                <thead>
                  <tr>
                    <th>Descripción</th>
                    <th>Código</th>
                    <th>Existencias</th>
                    <th>Marca</th>
                    <th>Categoría</th>
                    <th>Línea de producto</th>
                    <th>Fecha de registro</th>
                    <th>Estatus</th>
                  </tr>
                </thead>
                <tbody>';
        foreach ($productos as $index => $producto) {
            $render.= "<tr>";
            $render.= "<td>".$producto['descripcion']."</td>";
            $render.= "<td>".$producto['codigo']."</td>";
            $render.= "<td>".$producto['existencia']."</td>";
            $render.= "<td>".$producto['marca']."</td>";
            $render.= "<td>".$producto['categoria']."</td>";
            $render.= "<td>".$producto['lineaproducto']."</td>";
            $render.= "<td>".$producto['created_at']."</td>";
            $render.= "<td>".($producto['estatus']==1?'Activo':'Inactivo')."</td>";
            $render.= "</tr>";
        }
        $render.= "</tbody></table>";
        echo $render;
    }
}
