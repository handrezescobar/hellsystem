<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UsuariosController extends Controller{

	public function index(){
		return view('usuarios.index');
	}
    public function listar(){
    	$usuarios = User::all();
    	return $usuarios;
    }
    public function guardar(Request $request){
    	$data = $request->all();
    	if(isset($data['usuario_n'])){
    		$usuario_n = $data['usuario_n'];
    		$usuario = new User();
	    	$usuario->nombre = $usuario_n['nombre'];
	    	$usuario->apellido = $usuario_n['apellido'];
	    	$usuario->email = $usuario_n['email'];
	    	$usuario->password = bcrypt($data['password']);
	    	$usuario->save();	
    	} else {
    		$usuario_e = $data['usuario_e'];
    		$usuario = User::find($usuario_e['id']);
    		$usuario->nombre = $usuario_e['nombre'];
	    	$usuario->apellido = $usuario_e['apellido'];
	    	$usuario->email = $usuario_e['email'];
	    	$usuario->save();
    	}
    	echo "ok";
    }
    public function restaurarPassword(Request $request){
    	$data = $request->all();
    	if($data['passwords']['pass1'] == $data['passwords']['pass2']){
    		$usuario = User::find($data['usuario_id']);
    		$usuario->password = bcrypt($data['passwords']['pass1']);
    		$usuario->save();
    		echo "ok";
    	} else {
    		echo "Los passwords no coinciden";
    	}
    }
}
