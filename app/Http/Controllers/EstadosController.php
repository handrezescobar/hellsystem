<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Estado;
use App\Ciudad;

class EstadosController extends Controller
{
    //
    public function obtenerEstados(){
    	$estados = Estado::all()->toArray();
    	//dd($estados);
    	foreach ($estados as $index => $estado) {
    		$ciudades = Ciudad::where('estado_id',$estado['id'])->get()->toArray();
    		$estados[$index]['ciudades'] = $ciudades;
    	}
    	return $estados;
    }
}
