<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\LineaProducto;

class LineasProductosController extends Controller
{
  public function index(){
        return view('lineasproductos.index');
    } 
    public function listado(){
        $lineasproducto = LineaProducto::all();
        return $lineasproducto;
    } 
    public function guardar(Request $request){
      $data = $request->all();
        if($data['id'] == 0){
            $lineaproducto = new LineaProducto();
            $lineaproducto->nombre = $data['nombre'];
            $lineaproducto->abreviatura = strtoupper($data['abreviatura']);
            $lineaproducto->save();
        }else {
            $lineaproducto = LineaProducto::find($data['id']);
            $lineaproducto->nombre = $data['nombre'];
            $lineaproducto->abreviatura = strtoupper($data['abreviatura']);
            $lineaproducto->save();
        }
        echo "ok";
    }
    public function eliminar(Request $request){
        $data = $request->all();
        $lineaproducto = LineaProducto::find($data ['id']);
        $lineaproducto->delete();
        echo "ok";
    }
}
