<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Marca;

class MarcasController extends Controller
{
    public function index () {
    	return view('marcas.index');
    }
    public function listado(){
    	$marcas = Marca::all();
    	return $marcas;
    }

    public function guardar(Request $request){
    	$data = $request->all();
    	if($data ['id'] == 0){
    		$marca = new Marca();
    		$marca->nombre = $data['nombre'];
    		$marca->save();
    	} else {
    		$marca = marca::find($data['id']);
    		$marca->nombre = $data['nombre'];
    		$marca->save();
    	}
    	echo "ok";

    }
    public function eliminar(Request $request){
    	$data = $request ->all();
    	$Marca = Marca::find($data['id']);
    	$Marca->delete();
    	echo "ok";
    }
}
