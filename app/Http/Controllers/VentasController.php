<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Venta;
use App\ProductoVenta;
use App\Cliente;
use App\PrecioProveedor;
use App\Producto;
use DB;
use Illuminate\Support\Facades\Auth;
use PDO;

class VentasController extends Controller
{
    public function puntoCompra(){
    	return view('ventas.puntoventa');
    }
    public function resumen(){
		return view('ventas.resumen');
	}
	public function obtenerVentas(){
		$ventas = DB::select("SELECT v.id, v.cliente_id, CONCAT(c.nombre,' ',c.apellido) cliente, v.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, v.descporcentaje descuentop, v.descdinero descuentod, v.importe, v.subtotal, v.iva, v.total, v.fecha FROM ventas v INNER JOIN clientes c ON v.cliente_id = c.id INNER JOIN users u ON v.usuario_id = u.id");
		return $ventas;
	}
	public function obtenerVenta($venta_id){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$venta = DB::select("SELECT v.id, v.cliente_id, CONCAT(c.nombre,' ',c.apellido) cliente_nombre, c.rfc cliente_rfc, c.telefono cliente_telefono, c.email cliente_email, c.direccion cliente_direccion, c.codigopostal cliente_codigopostal,e.nombre estado, cd.nombre ciudad, v.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, u.email usuario_email, v.descporcentaje descuentop, v.descdinero descuentod, v.importe, v.subtotal, v.iva, v.total, v.fecha FROM ventas v INNER JOIN clientes c ON v.cliente_id = c.id INNER JOIN estados e ON c.estado_id = e.id INNER JOIN ciudades cd ON c.ciudad_id = cd.id INNER JOIN users u ON v.usuario_id = u.id WHERE v.id = $venta_id")[0];
		$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.nombre marca, c.nombre categoria, l.nombre linea, pv.costounitario precioventa, pv.cantidad, pv.importe FROM productos p INNER JOIN marcas m ON m.id = p.marca_id INNER JOIN categorias c ON c.id = p.categoria_id INNER JOIN lineasproducto l ON l.id = p.lineaproducto_id INNER JOIN productoventa pv ON p.id = pv.producto_id WHERE pv.venta_id = $venta_id");
		$venta['productos'] = $productos;
		return $venta;
	}
	public function obtenerProductosVenta(){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.id marca_id, m.nombre marca, c.id categoria_id, c.nombre categoria, l.id lineaproducto_id, l.nombre lineaproducto, p.estatus, p.existencia,MAX(pp.precio) precio,  p.updated_at FROM productos p INNER JOIN marcas m ON p.marca_id = m.id INNER JOIN categorias c ON p.categoria_id = c.id INNER JOIN  lineasproducto l ON p.lineaproducto_id = l.id INNER JOIN precioproveedor pp ON pp.producto_id = p.id WHERE p.estatus = true GROUP BY p.id");
		foreach ($productos as $index => $producto) {
			# code...
			$productos[$index]['precioventa'] = $producto['precio']+($producto['precio']*.30);
		}
		return $productos;
	}
	public function guardarVenta(Request $request){
		//dd($request->all());
		$data = $request->all();
		$user = Auth::user();
		$venta = new Venta();
		$venta->cliente_id = $data['cliente_id'];
		$venta->usuario_id = $user->id;
		$venta->importe = $data['importe'];
		$venta->descporcentaje = $data['descuentop'];
		$venta->descdinero = $data['descuentod'];
		$venta->subtotal = $data['subtotal'];
		$venta->iva = $data['iva'];
		$venta->total = $data['total'];
		$venta->fecha = date('Y-m-d H:i:s');
		$venta->estatus = TRUE;
		$venta->save();
		foreach ($data['productos'] as $index => $producto) {
			$pv = new ProductoVenta();
			$pv->producto_id = $producto['id'];
			$pv->venta_id = $venta->id;
			$pv->cantidad = $producto['cantidad'];
			$pv->costounitario = $producto['precioventa'];
			$pv->importe = $producto['importe'];
			$pv->save();
			$pr = Producto::find($producto['id']);
			$pr->existencia -= $producto['cantidad'];
			$pr->save();
		}
		echo $venta->id;
	}
	public function imprimir($venta_id){
		DB::setFetchMode(PDO::FETCH_ASSOC);
		$venta = DB::select("SELECT v.id, v.cliente_id, CONCAT(c.nombre,' ',c.apellido) cliente_nombre, c.rfc cliente_rfc, c.telefono cliente_telefono, c.email cliente_email, c.direccion cliente_direccion, c.codigopostal cliente_codigopostal,e.nombre estado, cd.nombre ciudad, v.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, u.email usuario_email, v.descporcentaje descuentop, v.descdinero descuentod, v.importe, v.subtotal, v.iva, v.total, v.fecha FROM ventas v INNER JOIN clientes c ON v.cliente_id = c.id INNER JOIN estados e ON c.estado_id = e.id INNER JOIN ciudades cd ON c.ciudad_id = cd.id INNER JOIN users u ON v.usuario_id = u.id WHERE v.id = $venta_id")[0];
		$productos = DB::select("SELECT p.id, p.descripcion, p.codigo, m.nombre marca, c.nombre categoria, l.nombre linea, pv.costounitario precioventa, pv.cantidad, pv.importe FROM productos p INNER JOIN marcas m ON m.id = p.marca_id INNER JOIN categorias c ON c.id = p.categoria_id INNER JOIN lineasproducto l ON l.id = p.lineaproducto_id INNER JOIN productoventa pv ON p.id = pv.producto_id WHERE pv.venta_id = $venta_id");
		return view('ventas.imprimir',['venta' => $venta, 'productos' => $productos]);
	}
	public function reporte(){
		header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=reporte_ventas_".date('Y-m-d').".xls");
        DB::setFetchMode(PDO::FETCH_ASSOC);
		$ventas = DB::select("SELECT v.id, v.cliente_id, CONCAT(c.nombre,' ',c.apellido) cliente, v.usuario_id, CONCAT(u.nombre,' ',u.apellido) usuario, v.descporcentaje descuentop, v.descdinero descuentod, v.importe, v.subtotal, v.iva, v.total, v.fecha FROM ventas v INNER JOIN clientes c ON v.cliente_id = c.id INNER JOIN users u ON v.usuario_id = u.id");
		$render = '<head>
                    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
                    </head>
            <table border="1">
                <thead>
                  <tr>
                    <th>Cliente</th>
                    <th>Importe</th>
                    <th>Descuento %</th>
                    <th>Descuento $</th>
                    <th>Subtotal</th>
                    <th>IVA</th>
                    <th>Total</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                  </tr>
                </thead>
                <tbody>';
        foreach ($ventas as $index => $venta) {
            $render.= "<tr>";
            $render.= "<td>".$venta['cliente']."</td>";
            $render.= "<td>".$venta['importe']."</td>";
            $render.= "<td>".$venta['descuentop']."</td>";
            $render.= "<td>".$venta['descuentod']."</td>";
            $render.= "<td>".$venta['subtotal']."</td>";
            $render.= "<td>".$venta['iva']."</td>";
            $render.= "<td>".$venta['total']."</td>";
            $render.= "<td>".$venta['usuario']."</td>";
            $render.= "<td>".$venta['fecha']."</td>";
            $render.= "</tr>";
        }
        $render.= "</tbody></table>";
        echo $render;
	}
}
