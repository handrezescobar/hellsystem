<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cliente;
use DB;

class ClientesController extends Controller
{
    public function index(){
    	return view('clientes.index');
    }

    public function listado(){
    	//$clientes = Cliente::all();
        $clientes = DB::select("SELECT cl.id, cl.nombre, cl.apellido, cl.email, cl.telefono, cl.direccion, cl.rfc,cl.codigopostal, c.id ciudad_id, c.nombre ciudad, e.id estado_id, e.nombre estado FROM clientes cl INNER JOIN ciudades c ON cl.ciudad_id = c.id INNER JOIN estados e ON cl.estado_id = e.id");
    	return $clientes;
    }

    public function guardar(Request $request){
    	//dd($request->all());
        $data = $request->all();
        if($data['id'] == 0){
            $cliente = new Cliente();
            $cliente->nombre = $data['nombre'];
            $cliente->apellido = $data['apellido'];
            $cliente->rfc = $data['rfc'];
            $cliente->email = $data['email'];
            $cliente->telefono = $data['telefono'];
            $cliente->direccion = $data['direccion'];
            $cliente->ciudad_id = $data['ciudad_id'];
            $cliente->estado_id = $data['estado_id'];
            $cliente->codigopostal = $data['codigopostal'];
            $cliente->save();
        } else {
            $cliente = Cliente::find($data['id']);
            $cliente->nombre = $data['nombre'];
            $cliente->apellido = $data['apellido'];
            $cliente->rfc = $data['rfc'];
            $cliente->email = $data['email'];
            $cliente->telefono = $data['telefono'];
            $cliente->direccion = $data['direccion'];
            $cliente->ciudad_id = $data['ciudad_id'];
            $cliente->estado_id = $data['estado_id'];
            $cliente->codigopostal = $data['codigopostal'];
            $cliente->save();
        }
        echo "ok";
    }
}
