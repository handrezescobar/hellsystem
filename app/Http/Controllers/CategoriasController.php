<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Categoria;

class CategoriasController extends Controller
{
    public function index(){
    	return view('categorias.index');
    }
    public function listado(){
    	$categorias = Categoria::all();
    	return $categorias;
    }
    public function guardar(Request $request){
    	$data = $request->all();
    	if($data['id'] == 0){
    		$categoria = new Categoria();
    		$categoria->nombre = $data['nombre'];
    		$categoria->save();
    	}else {
    		$categoria = Categoria::find($data['id']);
    		$categoria->nombre = $data['nombre'];
    		$categoria->save();
    	}
    	echo "ok";
    }
    public function eliminar(Request $request){
    	$data = $request->all();
    	$categoria = Categoria::find($data ['id']);
    	$categoria->delete();
    	echo "ok";
    }
}
