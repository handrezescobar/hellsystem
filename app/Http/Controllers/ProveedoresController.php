<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Proveedor;
use DB;

class ProveedoresController extends Controller
{
    public function index(){
    	return view('proveedores.index');
    }

    public function listado(){
    	//$proveedores = Proveedor::all();
        $proveedores = DB::select("SELECT p.id, p.nombre, p.representante, p.rfc, p.telefono, p.email, p.direccion, c.id ciudad_id, c.nombre ciudad, e.id estado_id, e.nombre estado, p.codigopostal FROM proveedores p INNER JOIN ciudades c ON p.ciudad_id = c.id INNER JOIN estados e ON e.id = p.estado_id");
    	return $proveedores;
    }

    public function guardar(Request $request){
    	$data = $request->all();
        //d($data['ciudad_id']);
    	if($data['id'] == 0){
    		$proveedor = new Proveedor();
    		$proveedor->nombre = $data['nombre'];
    		$proveedor->representante = $data['representante'];
    		$proveedor->rfc = $data['rfc'];
    		$proveedor->email = $data['email'];
    		$proveedor->telefono = $data['telefono'];
    		$proveedor->direccion = $data['direccion'];
    		$proveedor->ciudad_id = $data['ciudad_id'];
    		$proveedor->estado_id = $data['estado_id'];
    		$proveedor->codigopostal = $data['codigopostal'];
            //dd($proveedor);
    		$proveedor->save();
    	} else {
    		$proveedor = Proveedor::find($data['id']);
    		$proveedor->nombre = $data['nombre'];
    		$proveedor->representante = $data['representante'];
    		$proveedor->rfc = $data['rfc'];
    		$proveedor->email = $data['email'];
    		$proveedor->telefono = $data['telefono'];
    		$proveedor->direccion = $data['direccion'];
    		$proveedor->ciudad_id = $data['ciudad_id'];
    		$proveedor->estado_id = $data['estado_id'];
    		$proveedor->codigopostal = $data['codigopostal'];
    		$proveedor->save();
    	}
    	echo "ok";
    }
}
