<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    protected $table = "productos";

    protected $fillable = ['descripcion','codigo','categoria_id','marca_id','lineaproducto_id','estatus','existencia'];
}
