<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table = 'clientes';
    protected $fillable = ['nombre','apellido','email','telefono','rfc','direccion','ciudad_id','estado_id','codigopostal'];
}
