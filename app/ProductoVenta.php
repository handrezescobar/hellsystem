<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoVenta extends Model
{
    //
    protected $table = "productoventa";

    protected $fillable = ['producto_id','venta_id','cantidad','costounitario','importe'];
}
